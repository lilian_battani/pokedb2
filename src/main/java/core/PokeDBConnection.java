/**
 * 
 */
package core;

import java.io.*;
import java.sql.*;
import java.util.Objects;
import java.util.Properties;

import com.google.gson.JsonParser;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;

import static core.Resources.*;

/**
 * @author lilian battani
 * <p>Connection to pokedb database</p>
 *
 */
public class PokeDBConnection {

	/**
	 * unique PokeDBConnection instance
	 *
	 */
	private static PokeDBConnection INSTANCE;

	/**
	 * database connection
	 *
	 */
	private Connection connection;

	/**
	 *
	 */
	private MultiImporter m;

	/**
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 *
	 */
	private PokeDBConnection() throws IOException,SQLException{
		String url = "jdbc:sqlite:pokedb.db";

		this.connection=DriverManager.getConnection(url);
		this.connection.setAutoCommit(false);

		m=new MultiImporter();
	}

	/**
	 * @return the connection
	 *
	 */
	public Connection getDB(){
		return this.connection;
	}

	/**
	 *
	 * @return
	 */
	public SimpleBooleanProperty loadingProperty(){
		return m.loadingProperty();
	}

	/**
	 *
	 * @return
	 */
	public SimpleIntegerProperty loadedProperty(){
		return m.loadedProperty();
	}

	/**
	 *
	 * @return
	 */
	public int importerCount(){
		return m.importerCount();
	}

	/**
	 * reset the database
	 * @throws java.sql.SQLException
	 *
	 */
	public synchronized void reset() throws SQLException{
		PreparedStatement request=this.connection.prepareStatement("delete from type_table;\n" +
				"    delete from tutor_move;\n" +
				"    delete from level_move;\n" +
				"    delete from have_ability;\n" +
				"    delete from evolve;\n" +
				"    delete from egg_move;\n" +
				"    delete from dt_move;\n" +
				"    delete from ct_move;\n" +
				"    --\n" +
				"    delete from pokemon_move;\n" +
				"    delete from pokemon;\n" +
				"    delete from move;\n" +
				"    delete from ability;\n" +
				"    delete from egg_group;\n" +
				"    delete from type;");

		request.executeUpdate();
	}

	/**
	 * load the database with pokeapi data
	 *
	 */
	public synchronized void load() throws RuntimeException{
		m.reset();

		DataImporter abilities=new AbilityImporter();
       	DataImporter eggGroups=new EggGroupImporter();
       	DataImporter types=new TypeImporter();
       	DataImporter moves=new MoveImporter();
        DataImporter pokemons=new PokemonImporter();
        DataImporter ts=new TypeSpriteImporter();

       	m.add(abilities);
       	m.add(eggGroups);
       	m.add(types);
       	m.add(moves);
        m.add(pokemons);
        m.add(ts);

       	m.importData();
	}

	/**
	 * refresh the database
	 * @throws java.sql.SQLException
	 * @throws java.lang.RuntimeException
	 *
	 */
	public synchronized void refresh() throws SQLException,RuntimeException{
		Resources.backup();

		this.reset();
		this.load();
	}

	/**
	 *
	 * @throws SQLException
	 */
	public synchronized void create() throws SQLException,IOException{
		String crea = readAll("create_db.sql");

		Statement ps = null;

		for( String s : crea.split(";")) {
			try {
				if (s.equals("\n"))
					continue;

				ps = this.connection.createStatement();
				ps.execute(s + ";");
			}catch (SQLException sqle){}
		}

		this.connection.commit();

		File img_dir = new File("images/");

		ImageWriter.createImageDirIfInexistant();
		Resources.createBackupDirIfInexistant();
	}


	/**
	 * @return the unique PokeDBConnection instance
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 *
	 */
	public static synchronized PokeDBConnection getINSTANCE() throws IOException,SQLException,ClassNotFoundException{
		if( INSTANCE == null ){
			INSTANCE=new PokeDBConnection();
		}

		return INSTANCE;
	}


}
