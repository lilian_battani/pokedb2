package core;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 */
public class Scaler {

    /**
     *
     */
    public Scaler(){

    }

    /**
     *
     * @param img
     * @param iv
     * @param scale
     */
    public void add(ImageView iv,Image img, double scale){
        iv.setImage(img);

        iv.setFitWidth(img.getWidth()*scale);
        iv.setFitHeight(img.getHeight()*scale);
    }
}
