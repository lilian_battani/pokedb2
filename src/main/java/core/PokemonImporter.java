/**
 * 
 */
package core;

import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

import java.util.stream.Collectors;

import java.io.File;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Types;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.SQLException;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonArray;
import com.google.gson.JsonNull;

import jnet.apiclient.APIClient;
import jnet.apiclient.APIUrl;
import org.sqlite.SQLiteException;

import static core.DataImporter.*;
import static core.PokeAPIConnection.*;

/**
 * @author lilian battani
 * <p>Import pokemons</p>
 */
public class PokemonImporter extends DataImporter {

	/**
	 * pokemon sprites writer
	 *
	 */
	private ImageWriter imgWriter;

	/**
	 * 
	 *
	 */
	public PokemonImporter(){
		super();
		imgWriter=new ImageWriter();
	}

	/* (non-Javadoc)
	 * @see core.DataImporter#getData(core.PokeAPIConnection, core.PokeDBConnection)
	 */
	@Override
	public void getData(PokeAPIConnection api, PokeDBConnection db) {
		// TODO Auto-generated method stub
		try{
			JsonArray data=getPokemonData(api.getPokeApi());
			insertIntoDatabase(db.getDB(),data);
		}catch(SQLException se){
			throw new RuntimeException(se);
		}
	}

	/**
	 * @param pokeapi the api in which to get the data
	 * @return pokemons data
	 * @throws java.lang.RuntimeException 
	 *
	 */
	private JsonArray getPokemonData(APIClient pokeapi) throws RuntimeException{
		try{	
			APIUrl pokeurl=pokeapi.getAPIUrl();
			APIUrl url=null;
			boolean notNull=true;
			JsonArray pkmns=null;
			JsonObject p=null;
			String[] url_elements=null;
			int i=0;
			int j=0;
			boolean defForm=true;
			// pokemon data
			JsonArray pokemons=new JsonArray();
			JsonObject pokemon=null;
			JsonObject evolution_data=null;
			JsonObject form_data=null;
			// data
			String name=null;
			String fullName=null;
			String[] formName=null;
			String description=null;
			int[] egg_groups=new int[2];
			Map<Integer,List<Integer>> evolutions=null;
			//
			pokeurl.setHasFile(false);
			pokeurl.addToPath("pokemon-species").addToPath("?offset=0&limit=20");
			pokeapi.updateConnection();
			JsonObject e=pokeapi.get().getAsJsonObject();
			// 
			while( notNull ){
				pokeurl.setHasFile(true);
				pokeurl.delFromPath();

				notNull=!(e.get("next").isJsonNull());
				pkmns=e.get("results").getAsJsonArray();

				for( JsonElement po : pkmns ) {
					try{
						this.reset();

						url = new APIUrl(po.getAsJsonObject().get("url").getAsString());

						pokeurl.addToPath("pokemon-species").addToPath(url.getPath().getLast());
						pokeapi.updateConnection();
						p = pokeapi.get().getAsJsonObject();

						egg_groups[0] = -1;
						egg_groups[1] = -1;

						// pokemon name and description
						i = searchWithLanguage(p.get("names").getAsJsonArray());

						if (i != -1)
							name = p.get("names").getAsJsonArray().get(i).getAsJsonObject().get("name").getAsString();
						else
							name = p.get("name").getAsString();

						j = searchWithLanguage(p.get("flavor_text_entries").getAsJsonArray(), -1);

						if (j != -1)
							description = p.get("flavor_text_entries").getAsJsonArray().get(j).getAsJsonObject().get("flavor_text").getAsString();
						else
							description = "";

						// egg group
						for (int l = 0; l < p.get("egg_groups").getAsJsonArray().size() && l < 2; l++) {
							url = new APIUrl(p.get("egg_groups").getAsJsonArray().get(l).getAsJsonObject().get("url").getAsString());
							egg_groups[l] = Integer.parseInt(url.getPath().getLast());
						}

						// evolutions
						this.reset();
						url = new APIUrl(p.get("evolution_chain").getAsJsonObject().get("url").getAsString());

						for (int m = url.getPath().indexOf(ROOT) + 1; m < url.getPath().size(); m++)
							pokeurl.addToPath(url.getPath().get(m));

						pokeapi.updateConnection();
						evolution_data = pokeapi.get().getAsJsonObject();
						evolutions = this.getEvolutions(pokeapi, evolution_data.get("chain").getAsJsonObject());
						//
						this.reset();
						pokeurl.addToPath("pokemon");
						// add pokemon form separately
						for (JsonElement form : p.get("varieties").getAsJsonArray()) {
							url = new APIUrl(form.getAsJsonObject().get("pokemon").getAsJsonObject().get("url").getAsString());
							pokeurl.addToPath(url.getPath().getLast());
							pokeapi.updateConnection();
							form_data = pokeapi.get().getAsJsonObject();
							//
							pokemon = new JsonObject();
							pokemons.add(pokemon);
							//
							defForm = form_data.get("is_default").getAsBoolean();
							//pokemon id
							pokemon.addProperty("id", form_data.get("id").getAsInt());
							//pokemon name
							if (defForm)
								pokemon.addProperty("name", name);
							else {
								formName = form_data.get("name").getAsString().split("-");

								fullName = name;
								boolean first = true;

								for(String part : formName ) {
									if( ! first )
										fullName += "-" + part;
									else
										first = false;
								}

								pokemon.addProperty("name", fullName);
							}
							//pokemon description
							if (defForm)
								pokemon.addProperty("description", description);
							else
								pokemon.addProperty("description", "forme " + (formName.length > 1 ? formName[1] : "") + " de " + name + "\n" + description);

							// pokemon types
							pokemon.add("types", new JsonArray());

							for (JsonElement typ : form_data.get("types").getAsJsonArray()) {
								url = new APIUrl(typ.getAsJsonObject().get("type").getAsJsonObject().get("url").getAsString());
								pokemon.get("types").getAsJsonArray().add(Integer.parseInt(url.getPath().getLast()));
							}

							// pokemon egg groups
							pokemon.add("egg_groups", new JsonArray());

							for (Integer eggGrp : egg_groups)
								if (eggGrp != -1)
									pokemon.get("egg_groups").getAsJsonArray().add(eggGrp);

							// pokemon sprite
							if (!form_data.get("sprites").getAsJsonObject().get("other").isJsonNull() && !form_data.get("sprites").getAsJsonObject().get("other").getAsJsonObject().get("official-artwork").isJsonNull() && !form_data.get("sprites").getAsJsonObject().get("other").getAsJsonObject().get("official-artwork").getAsJsonObject().get("front_default").isJsonNull())
								pokemon.addProperty("sprite", form_data.get("sprites").getAsJsonObject().get("front_default").getAsString());
							else
								pokemon.add("sprite", new JsonNull());

							// pokemon statistics
							pokemon.addProperty("speed", this.getStat(form_data.get("stats").getAsJsonArray(), "speed"));
							pokemon.addProperty("speDef", this.getStat(form_data.get("stats").getAsJsonArray(), "special-defense"));
							pokemon.addProperty("speAtk", this.getStat(form_data.get("stats").getAsJsonArray(), "special-attack"));
							pokemon.addProperty("defense", this.getStat(form_data.get("stats").getAsJsonArray(), "defense"));
							pokemon.addProperty("attack", this.getStat(form_data.get("stats").getAsJsonArray(), "attack"));
							pokemon.addProperty("hp", this.getStat(form_data.get("stats").getAsJsonArray(), "hp"));

							// pokemon moves
							pokemon.add("egg_moves", new JsonArray());
							pokemon.add("ct_moves", new JsonArray());
							pokemon.add("dt_moves", new JsonArray());
							pokemon.add("level_moves", new JsonArray());
							pokemon.add("tutor_moves", new JsonArray());

							this.insertMoves(form_data.get("moves").getAsJsonArray(), pokemon);

							// pokemon abilities
							pokemon.add("abilities", new JsonArray());

							this.insertAbilities(form_data.get("abilities").getAsJsonArray(), pokemon);

							// pokemon evolutions
							pokemon.add("evolutions", new JsonArray());

							this.insertEvolutions(evolutions, pokemon);

							//
							pokeurl.delFromPath();
						}
					}catch (Exception ex){
						continue;
					}
				}

				pokeurl.setHasFile(false);

				if( notNull ){
					this.reset();
					pokeurl.addToPath("pokemon-species");

					url_elements=e.get("next").getAsString().split("/");
					pokeurl.addToPath(url_elements[url_elements.length-1]);
					pokeapi.updateConnection();
					e=pokeapi.get().getAsJsonObject();
				}
			}

			return pokemons; 
		}catch(Exception e){
			throw new RuntimeException(e);
		}finally{
			this.reset();
		}
	}

	/**
	 * @param pokeapi the api client
	 * @param evolutions json element in which to search evolution informations
	 * @return evolution conditions
	 * @throws java.lang.RuntimeException
	 *
	 */
	private Map<Integer,List<Integer>> getEvolutions(APIClient pokeapi,JsonObject evolutions){
		try{
			Map<Integer,List<Integer>> evo=new HashMap<>();
			LinkedList<String> previousUrl=new LinkedList<>();
			List<Integer> evolveTo=new ArrayList<>();
			APIUrl pokeurl=pokeapi.getAPIUrl();
			APIUrl url=null;
			JsonObject evolution=null;
			int id=0;
			int id2=0;
			boolean notGoodSpecy=true;

			// save previous url
			for(int i=pokeurl.getPath().size()-1;i>=0;i--)
				if( pokeurl.getPath().get(i).equals(ROOT) )
					break;
				else
					previousUrl.addLast(pokeurl.getPath().get(i));

			this.reset();
			//
			url=new APIUrl(evolutions.get("species").getAsJsonObject().get("url").getAsString());
			id=Integer.parseInt(url.getPath().getLast()); 
			evo.put(id,evolveTo);
			//
			for( JsonElement evol : evolutions.get("evolves_to").getAsJsonArray() ){
				evolution=evol.getAsJsonObject();
				// retrieve evolution id
				url=new APIUrl(evolution.get("species").getAsJsonObject().get("url").getAsString());
				id2=Integer.parseInt(url.getPath().getLast());
				evolveTo.add(id2);
				//
				getEvolutions(pokeapi,evolution).forEach( (k,v) -> {
					evo.put(k,v);
				});
				// add evolution conditions later 
			}
			//
			this.reset();

			for( String u : previousUrl )
				pokeurl.addToPath(u); 
			// 
			return evo; 
			
		}catch(Exception e){
			throw new RuntimeException(e);
		}
	}

	/**
	 * @param stats JsonArray in which to search a stat
	 * @param statName name of the stat to find
	 * @return value of the found stat or -1 if not found
	 *
	 */
	private int getStat(JsonArray stats,String statName){
		if( statName == null )
			return -1;

		for( JsonElement statistic : stats )
			if( statistic.getAsJsonObject().get("stat").getAsJsonObject().get("name").getAsString().equals(statName) )
				return statistic.getAsJsonObject().get("base_stat").getAsInt();

		return -1;
	}

	/**
	 * @param moves pokemon moves array
	 * @param pokemon json object representing the pokemon in which to add his moves
	 * @throws java.lang.RuntimeException
	 *
	 */
	private void insertMoves(JsonArray moves,JsonObject pokemon) throws RuntimeException{
		try{
			JsonArray egg_moves=pokemon.get("egg_moves").getAsJsonArray();
			JsonArray ct_moves=pokemon.get("ct_moves").getAsJsonArray();
			JsonArray dt_moves=pokemon.get("dt_moves").getAsJsonArray();
			JsonArray level_moves=pokemon.get("level_moves").getAsJsonArray();
			JsonArray tutor_moves=pokemon.get("tutor_moves").getAsJsonArray();
			//
			JsonObject move=null;
			JsonArray group_details=null;
			APIUrl url=null;
			//
			for( JsonElement mo : moves ){
				move=new JsonObject();
				// 
				url=new APIUrl(mo.getAsJsonObject().get("move").getAsJsonObject().get("url").getAsString());
				move.addProperty("id",Integer.parseInt(url.getPath().getLast()));
				group_details=mo.getAsJsonObject().get("version_group_details").getAsJsonArray();

				if( group_details.size() <= 0 )
					continue;

				switch( group_details.get(0).getAsJsonObject().get("move_learn_method").getAsJsonObject().get("name").getAsString() ){
					case "level-up":
						move.addProperty("level",group_details.get(0).getAsJsonObject().get("level_learned_at").getAsInt());
						level_moves.add(move);
					break;

					case "form-change":
						move.addProperty("level",0);
						level_moves.add(move);
					break;

					case "egg":
						egg_moves.add(move);
					break;

					case "tutor":
						tutor_moves.add(move);
					break;

					case "machine":
						ct_moves.add(move);
					break;
				}
			}
		}catch(Exception e){
			throw new RuntimeException(e);
		}
	}

	/**
	 * @param abilities pokemon abilities array
	 * @param pokemon json object representing the pokemon in which to add his abilities
	 * @throws java.lang.RuntimeException
	 *
	 */
	private void insertAbilities(JsonArray abilities,JsonObject pokemon) throws RuntimeException{
		try{
			JsonArray abs=pokemon.get("abilities").getAsJsonArray();
			JsonObject ability=null;
			APIUrl url=null;

			for( JsonElement pokemon_ability : abilities ){
				ability=new JsonObject();
				url=new APIUrl(pokemon_ability.getAsJsonObject().get("ability").getAsJsonObject().get("url").getAsString());

				ability.addProperty("id",Integer.parseInt(url.getPath().getLast()));
				ability.addProperty("hidden",pokemon_ability.getAsJsonObject().get("is_hidden").getAsBoolean());
				// 
				abs.add(ability);
			}
		}catch(Exception e){
			throw new RuntimeException(e);
		}
	}

	/**
	 * @param evolutions pokemon evolutions
	 * @param pokemon json object representing the pokemon in which to add his moves
	 * @throws java.lang.RuntimeException
	 *
	 */
	private void insertEvolutions(Map<Integer,List<Integer>> evolutions,JsonObject pokemon) throws RuntimeException{
		try{
			JsonArray evos=pokemon.get("evolutions").getAsJsonArray();
			JsonArray evolveTo=null;
			JsonObject evolution=null;

			for( Integer id : evolutions.entrySet().stream().map( e -> e.getKey() ).collect(Collectors.toList()) ){
				evolution=new JsonObject();
				evolveTo=new JsonArray();
				// 
				evolution.addProperty("id",id);
				evolution.add("evolveTo",evolveTo);

				for( Integer k : evolutions.get(id) )
					evolveTo.add(k);
				// 
				evos.add(evolution);
			}
		}catch(Exception e){
			throw new RuntimeException(e);
		}
	}

	/**
	 * insert pokemons data in database
	 * @param db connection to the database
	 * @param pokemons pokemons data
	 * @throws java.sql.SQLException
	 *
	 */
	private void insertIntoDatabase(Connection db,JsonArray pokemons) throws SQLException{
		JsonObject pokemon=null;
		PreparedStatement request=null;
		String sprite=null;
		File spriteImage=null;
		Map<Integer,List<Integer>> evolutions=new HashMap<>();
		List<Integer> t=null;
		// fill pokemon table
		for( JsonElement p : pokemons ){
			pokemon=p.getAsJsonObject();

			try{
				if( ! pokemon.get("sprite").isJsonNull() ) {
					sprite = this.imgWriter.writeImage(pokemon.get("sprite").getAsString(), pokemon.get("name").getAsString());
				}else {
					sprite = null;
				}
			}catch(NullPointerException npe){
				sprite=null;
			}

			try{
				if( pokemon.get("types").getAsJsonArray().size() <= 0 )
					continue;

				request=db.prepareStatement("insert into pokemon values(?,?,?,?,?,?,?,?,?,?,?,?,?,?);");
				//
				request.setInt(1,pokemon.get("id").getAsInt());
				request.setString(2,pokemon.get("name").getAsString());
				request.setInt(3,pokemon.get("hp").getAsInt());
				request.setInt(4,pokemon.get("attack").getAsInt());
				request.setInt(5,pokemon.get("defense").getAsInt());
				request.setInt(6,pokemon.get("speAtk").getAsInt());
				request.setInt(7,pokemon.get("speDef").getAsInt());
				request.setInt(8,pokemon.get("speed").getAsInt());
				//
				request.setInt(9,pokemon.get("types").getAsJsonArray().get(0).getAsInt()); 

				if( pokemon.get("types").getAsJsonArray().size() > 1 )
					request.setInt(10,pokemon.get("types").getAsJsonArray().get(1).getAsInt()); 
				else
					request.setNull(10,Types.INTEGER); 

				if( pokemon.get("egg_groups").getAsJsonArray().size() > 0 )
					request.setInt(11,pokemon.get("egg_groups").getAsJsonArray().get(0).getAsInt()); 
				else
					request.setNull(11,Types.INTEGER); 

				if( pokemon.get("egg_groups").getAsJsonArray().size() > 1 )
					request.setInt(12,pokemon.get("egg_groups").getAsJsonArray().get(1).getAsInt()); 
				else
					request.setNull(12,Types.INTEGER);

				if( sprite != null )
					request.setString(13,sprite);
				else
					request.setNull(13,Types.VARCHAR);

				request.setString(14,pokemon.get("description").getAsString());
				// 
				request.executeUpdate();
				// fill pokemon dependants tables
				// pokemon abilities
				for( JsonElement abilityElement : pokemon.get("abilities").getAsJsonArray() ){
					request=db.prepareStatement("insert into have_ability values (?,?,?);");

					request.setInt(1,pokemon.get("id").getAsInt());
					request.setInt(2,abilityElement.getAsJsonObject().get("id").getAsInt());
					request.setBoolean(3,abilityElement.getAsJsonObject().get("hidden").getAsBoolean());

					request.executeUpdate();
				}

				// pokemon moves
				// level moves
				for( JsonElement move : pokemon.get("level_moves").getAsJsonArray() ){
					request=db.prepareStatement("insert into pokemon_move values (?,?);");

					request.setInt(1,pokemon.get("id").getAsInt());
					request.setInt(2,move.getAsJsonObject().get("id").getAsInt());

					request.executeUpdate();

					request=db.prepareStatement("insert into level_move values (?,?,?);");

					request.setInt(1,pokemon.get("id").getAsInt());
					request.setInt(2,move.getAsJsonObject().get("id").getAsInt());
					request.setInt(3,move.getAsJsonObject().get("level").getAsInt());

					request.executeUpdate();
				}

				// egg moves
				for( JsonElement move : pokemon.get("egg_moves").getAsJsonArray() ){
					try{
						request=db.prepareStatement("insert into pokemon_move values (?,?);");

						request.setInt(1,pokemon.get("id").getAsInt());
						request.setInt(2,move.getAsJsonObject().get("id").getAsInt());

						request.executeUpdate();
					}catch(SQLiteException se){}

					request=db.prepareStatement("insert into egg_move values (?,?);");

					request.setInt(1,pokemon.get("id").getAsInt());
					request.setInt(2,move.getAsJsonObject().get("id").getAsInt());

					request.executeUpdate();
				}

				// ct moves
				for( JsonElement move : pokemon.get("ct_moves").getAsJsonArray() ){
					try{
						request=db.prepareStatement("insert into pokemon_move values (?,?);");

						request.setInt(1,pokemon.get("id").getAsInt());
						request.setInt(2,move.getAsJsonObject().get("id").getAsInt());

						request.executeUpdate();
					}catch(SQLiteException se){}

					request=db.prepareStatement("insert into ct_move values (?,?);");

					request.setInt(1,pokemon.get("id").getAsInt());
					request.setInt(2,move.getAsJsonObject().get("id").getAsInt());

					request.executeUpdate();
				}

				// dt moves
				for( JsonElement move : pokemon.get("dt_moves").getAsJsonArray() ){
					try{
						request=db.prepareStatement("insert into pokemon_move values (?,?);");

						request.setInt(1,pokemon.get("id").getAsInt());
						request.setInt(2,move.getAsJsonObject().get("id").getAsInt());

						request.executeUpdate();
					}catch(SQLiteException se){}

					request=db.prepareStatement("insert into dt_move values (?,?);");

					request.setInt(1,pokemon.get("id").getAsInt());
					request.setInt(2,move.getAsJsonObject().get("id").getAsInt());

					request.executeUpdate();
				}

				// tutor moves
				for( JsonElement move : pokemon.get("tutor_moves").getAsJsonArray() ){
					try{
						request=db.prepareStatement("insert into pokemon_move values (?,?);");

						request.setInt(1,pokemon.get("id").getAsInt());
						request.setInt(2,move.getAsJsonObject().get("id").getAsInt());

						request.executeUpdate();
					}catch(SQLiteException se){}

					request=db.prepareStatement("insert into tutor_move values (?,?);");

					request.setInt(1,pokemon.get("id").getAsInt());
					request.setInt(2,move.getAsJsonObject().get("id").getAsInt());

					request.executeUpdate();
				}

				// evolutions
				for( JsonElement evo : pokemon.get("evolutions").getAsJsonArray() )
					if( ! evolutions.containsKey(evo.getAsJsonObject().get("id").getAsInt()) ){
						t=new ArrayList<>();
						evolutions.put(evo.getAsJsonObject().get("id").getAsInt(),t);

						for( JsonElement evTo : evo.getAsJsonObject().get("evolveTo").getAsJsonArray() )
							t.add(evTo.getAsInt());
					}
				

				//
				db.commit();
				// 
			}catch(SQLiteException se){
				db.rollback();
			}
		}

		for( Integer from : evolutions.entrySet().stream().map( e -> e.getKey() ).collect(Collectors.toList()) ){
			for( Integer to : evolutions.get(from) )
				try{
					request=db.prepareStatement("insert into evolve values(?,?,?)");

					request.setInt(1,from);
					request.setInt(2,to);
					request.setString(3,"");

					request.executeUpdate();
				}catch(SQLiteException se){}
		}

		db.commit();
	}

}
