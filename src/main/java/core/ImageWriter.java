/**
 * 
 */
package core;

import java.io.IOException;
import java.io.File;

import java.io.InputStream;
import java.net.URL;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;

import javax.imageio.ImageIO;

import java.awt.image.BufferedImage;

/**
 * @author lilian battani
 *
 */
public class ImageWriter {

	/**
	 * default image format
	 *
	 */
	public final static String DEFAULT_FORMAT="png";

	/**
	 * path to images folder
	 *
	 */
	public final static String PATH="images/";

	/**
	 *
	 *
	 */
	public ImageWriter(){}

	/**
	 * write image in images folder from a given url
	 * @param url the URL
	 * @param name the name of the image to create
	 * @return name of the created image or null if an error occured
	 * @throws java.lang.NullPointerException if url or name is null
	 *
	 */
	public synchronized String writeImage(String url,String name) throws NullPointerException{
		if( url == null || name == null )
			throw new NullPointerException("null");

		String[] nameTab=name.split("\\.");
		String extension=( (nameTab.length > 1) ? nameTab[nameTab.length-1] : DEFAULT_FORMAT );
		String fileName=PATH+name+( (nameTab.length > 1) ? "" : "."+extension);
		URL u=null;
		HttpURLConnection connection=null;

		try{
			u=new URL(url);
		}catch(MalformedURLException mue){
			mue.printStackTrace();
			return null;
		}

		try{
			connection=(HttpURLConnection)u.openConnection();
		}catch(IOException ioe){
			ioe.printStackTrace();
			return null;
		}

		try{
			connection.setRequestProperty("User-Agent","Mozilla/5.0");
			connection.setRequestMethod("GET");
			connection.setDoOutput(true);
			connection.connect();
		}catch(ProtocolException pe){
			pe.printStackTrace();
			return null;
		}catch(IOException ioe){
			ioe.printStackTrace();
			return null;
		}

		BufferedImage image = null;

		try{
			image = ImageIO.read(connection.getInputStream());
		}catch(IOException ioe){
			ioe.printStackTrace();
			return null;
		}
		// end of connection preparation

		try{
			File f=new File(fileName);
			ImageIO.write(image,extension,f);
		}catch(IOException ioe){
			ioe.printStackTrace();
			return null;
		}

		return fileName;
	}

	/**
	 * write an image from an url
	 * @param url source from which to copy the image
	 * @param name name of the file to create
	 * @return name of the created image or null if an error occured
	 * @throws NullPointerException if url or name is null
	 */
	public synchronized String writeImage(InputStream url, String name) throws NullPointerException{
		if( url == null || name == null )
			throw new NullPointerException("null");

		String[] nameTab=name.split("\\.");
		String extension=( (nameTab.length > 1) ? nameTab[nameTab.length-1] : DEFAULT_FORMAT );
		String fileName=PATH+name+( (nameTab.length > 1) ? "" : "."+extension);
		BufferedImage image = null;

		try{
			image = ImageIO.read(url);
		}catch(IOException ioe){
			ioe.printStackTrace();
			return null;
		}

		try{
			File f=new File(fileName);
			ImageIO.write(image,extension,f);
		}catch(IOException ioe){
			ioe.printStackTrace();
			return null;
		}
		//
		return fileName;
	}

	/**
	 *
	 */
	public static void createImageDirIfInexistant(){
		File f = new File(PATH);

		if( ! f.exists() )
			f.mkdir();
		else{
			if( ! f.isDirectory() ) {
				f.delete();
				f.mkdir();
			}
		}
	}
}
