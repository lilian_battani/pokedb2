package core;

import javafx.scene.control.Alert;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Objects;

public class Resources {

    /**
     *
     */
    public final static String BACKUP_DIR = "backup/";

    /**
     * read a file entirely
     * @param is
     * @return file content (\n included)
     * @throws IOException
     */
    public static String readAll(String is) throws IOException{
        String s= "";
        String tmp;
        BufferedReader br=new BufferedReader(new InputStreamReader(Objects.requireNonNull(PokeDBConnection.class.getClassLoader().getResourceAsStream(is))));

        while( (tmp = br.readLine()) != null )
            s+=tmp+"\n";

        return s;
    }

    /**
     *
     */
    public static void createBackupDirIfInexistant(){
        File f = new File(BACKUP_DIR);

        if( ! f.exists() )
            f.mkdir();
        else{
            if( ! f.isDirectory() ) {
                f.delete();
                f.mkdir();
            }
        }
    }

    /**
     *
     */
    public static void backup(){
        File db = new File("pokedb.db");
        File backup = null;
        Path backup_file = null;
        Path db_path = db.toPath();

        int i = 0;

        backup = new File(BACKUP_DIR+"pokedb"+i+".db");
        backup_file = backup.toPath();

        while( backup.exists() ){
            i++;
            backup = new File(BACKUP_DIR+"pokedb"+i+".db");
            backup_file = backup.toPath();
        }

        // an inexistant file has been found and we can create it
        try {
            Files.copy(db_path, backup_file, StandardCopyOption.REPLACE_EXISTING);
        }catch(IOException ioe){
            Alert a = new Alert(Alert.AlertType.ERROR,"error while creating backup\n"+ioe.getMessage());
            a.showAndWait();
        }
    }
}
