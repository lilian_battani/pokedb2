/**
 * 
 */
package core;

import java.net.MalformedURLException;

import jnet.apiclient.APIClient;

/**
 * @author lilian battani
 * <p>Unique connection to poke api</p>
 *
 */
public class PokeAPIConnection {

	/**
	 * path root
	 *
	 */
	public final static String ROOT="v2";

	/**
	 * PokeAPIConnection unique instance
	 *
	 */
	private static PokeAPIConnection INSTANCE;

	/**
	 * connection to api
	 *
	 */
	private APIClient pokeApi;

	/**
	 * @throws java.net.MalformedURLException
	 *
	 */
	private PokeAPIConnection() throws MalformedURLException{
		this.pokeApi=new APIClient("https://pokeapi.co/api/v2/");
	}

	/**
	 * @return the pokeApi
	 *
	 */
	public APIClient getPokeApi(){
		return this.pokeApi;
	}

	/**
	 * @return PokeAPIConnection unique instance
	 * @throws java.net.MalformedURLException
	 *
	 */
	public static synchronized PokeAPIConnection getINSTANCE() throws MalformedURLException{
		if( INSTANCE == null )
			INSTANCE=new PokeAPIConnection();

		return INSTANCE;
	}
}
