/**
 * 
 */
package core;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLIntegrityConstraintViolationException;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonArray;

import jnet.apiclient.APIClient;
import jnet.apiclient.APIUrl;
import org.sqlite.SQLiteException;

import static core.DataImporter.*;

/**
 * @author lilian battani
 * <p>Import pokemon moves</p>
 *
 */
public class MoveImporter extends DataImporter {

	/**
	 *
	 *
	 */
	public MoveImporter(){
		super();
	}

	/* (non-Javadoc)
	 * @see core.DataImporter#getData(core.PokeAPIConnection, core.PokeDBConnection)
	 */
	@Override
	public void getData(PokeAPIConnection api, PokeDBConnection db) {
		try{
		// TODO Auto-generated method stub
			APIClient pokeapi=api.getPokeApi();
			APIUrl pokeurl=pokeapi.getAPIUrl();
			Connection pokedb=db.getDB();

			pokeurl.setHasFile(false);
			pokeurl.addToPath("move").addToPath("?offset=0&limit=20");
			pokeapi.updateConnection();

			// initializations
			PreparedStatement request=null;
			JsonObject e=pokeapi.get().getAsJsonObject();
			JsonObject move=null;
			JsonArray moves=null;
			JsonArray names=null;
			JsonArray descriptions=null;
			String[] url_elements=null;
			APIUrl move_url=null;
			String url=null;
			boolean notNull=true;
			// 
			int moveId=0;
			String moveName=null;
			String moveDescription=null;
			int movePower=0;
			int moveAccuracy=0;
			int movePp=0;
			int moveStatus=0;
			int moveTypeId=0;
			// 
			int j=0;
			int k=0;

			while( notNull ){
				pokeurl.setHasFile(true);
				pokeurl.delFromPath();

				notNull=!(e.get("next").isJsonNull());
				moves=e.get("results").getAsJsonArray();

				for( JsonElement jse : moves ){
					// load the ability designated by i
					url=jse.getAsJsonObject().get("url").getAsString();
					move_url=new APIUrl(url);

					pokeurl.addToPath(move_url.getPath().getLast());
					pokeapi.updateConnection();
					move=pokeapi.get().getAsJsonObject();
					//retrieve data from json 
					names=move.get("names").getAsJsonArray();
					descriptions=move.get("flavor_text_entries").getAsJsonArray();
					
					j=searchWithLanguage(names);
					k=searchWithLanguage(descriptions);

					// ability data 
					moveId=move.get("id").getAsInt();

					if( j != -1 )
						moveName=names.get(j).getAsJsonObject().get("name").getAsString();
					else
						moveName=move.get("name").getAsString();

					if( k != -1 )
						moveDescription=descriptions.get(k).getAsJsonObject().get("flavor_text").getAsString();
					else if( move.get("effect_entries").getAsJsonArray().size() > 0 )
						moveDescription=move.get("effect_entries").getAsJsonArray().get(0).getAsJsonObject().get("effect").getAsString();
					else
						moveDescription="";

					if( ! move.get("power").isJsonNull() )
						movePower=move.get("power").getAsInt();
					else
						movePower=0;

					if( ! move.get("accuracy").isJsonNull() )
						moveAccuracy=move.get("accuracy").getAsInt();
					else
						moveAccuracy=100;

					if( ! move.get("pp").isJsonNull() )
						movePp=move.get("pp").getAsInt();
					else
						movePp=1;

					if( move.get("damage_class").isJsonNull() ){
						pokeurl.delFromPath();
						continue;
					}

					moveStatus=categoryToInt(move.get("damage_class").getAsJsonObject().get("name").getAsString());

					// move type retrieving
					move_url=new APIUrl(move.get("type").getAsJsonObject().get("url").getAsString());
					moveTypeId=Integer.parseInt(move_url.getPath().getLast());

					// insert data into the database

					try{
						request=pokedb.prepareStatement(
							"insert into move(id,name,description,power,accuracy,pp,status,type) values(?,?,?,?,?,?,?,?);"
						);

						request.setInt(1,moveId);
						request.setString(2,moveName);
						request.setString(3,moveDescription);
						request.setInt(4,movePower);
						request.setInt(5,moveAccuracy);
						request.setInt(6,movePp);
						request.setInt(7,moveStatus);
						request.setInt(8,moveTypeId);

						request.executeUpdate();
					}catch(SQLiteException se){}
					//
					pokeurl.delFromPath();
				}

				pokeurl.setHasFile(false);

				if( notNull ){
					url_elements=e.get("next").getAsString().split("/");
					pokeurl.addToPath(url_elements[url_elements.length-1]);
					pokeapi.updateConnection();
					e=pokeapi.get().getAsJsonObject();
				}
			}

			// save changes
			pokedb.commit();

		}catch(Exception e){
			throw new RuntimeException(e);
		}finally{
			this.reset();
		}
	}

	/**
	 * convert move category to int
	 * @param category the category to convert 
	 *
	 */
	private static int categoryToInt(String category){
		switch(category){
			case "physical":
				return 0;
			case "special":
				return 1;
			case "status":
				return 2;
			default:
				return -1;
		}
	}
}

