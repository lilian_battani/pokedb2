/**
 * 
 */
package core;

import com.google.gson.JsonArray;

import jnet.apiclient.APIUrl;

/**
 * @author lilian battani
 * <p>Import data from pokeapi to pokedb</p>
 *
 */
public abstract class DataImporter {

	/**
	 * language of the imported data
	 *
	 */
	public final static String LANGUAGE="fr";

	/**
	 *
	 *
	 */
	public DataImporter(){}

	/**
	 * run data import
	 * @throws java.lang.RuntimeException if an exception occurs while importing the data
	 */
	public void importData(){
		try{
			PokeAPIConnection pokeapi=PokeAPIConnection.getINSTANCE();
			PokeDBConnection pokedb=PokeDBConnection.getINSTANCE();

			this.getData(pokeapi,pokedb);
		}catch(Exception e){
			throw new RuntimeException(e);
		}
	}

	/**
	 * reset api url to the root
	 * @throws java.lang.RuntimeException if an exception occurs while reseting
	 */
	public void reset(){
		try{
			APIUrl url=PokeAPIConnection.getINSTANCE().getPokeApi().getAPIUrl();

			while( ! url.getPath().getLast().equals( PokeAPIConnection.ROOT ) )
				url.delFromPath();
			
		}catch(Exception e){
			throw new RuntimeException(e);
		}
	}

	/**
	 * get data from the api and insert them into the database
	 * @param api connection to pokeapi
	 * @param db connection to the database
	 *
	 */
	public abstract void getData(PokeAPIConnection api,PokeDBConnection db);

	/**
	 * @param a json array in which to do the search
	 * @return the index of the element of language LANGUAGE in a jsonarray or -1 if not found 
	 *
	 */
	static int searchWithLanguage(JsonArray a){
		int j=0;
		boolean notGoodLanguage=true;

		while( j < a.size() && notGoodLanguage )
			if( ! a.get(j).getAsJsonObject().get("language").getAsJsonObject().get("name").getAsString().equals(DataImporter.LANGUAGE) )
				j++;
			else
				notGoodLanguage=false;

		if( notGoodLanguage )
			return -1;
		else
			return j;
	}

	/**
	 * @param a json array in which to do the search
	 * @param i number of occurence ( if i = -1 the index of the last occurence will be returned)
	 * @return the index of the element of language LANGUAGE in a jsonarray or -1 if not found 
	 *
	 */
	static int searchWithLanguage(JsonArray a,int i){
		int j=0;
		int k=-1;
		int occurrence=0;

		while( j < a.size() && ( occurrence < i || i == -1 ) ){
			if( a.get(j).getAsJsonObject().get("language").getAsJsonObject().get("name").getAsString().equals(DataImporter.LANGUAGE) ){			
				occurrence++;
				k=j;
			}

			j++;
		}

		return k;
	}
}
