/**
 * 
 */
package core;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLIntegrityConstraintViolationException;

import org.sqlite.SQLiteException;

import com.google.gson.JsonElement;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import jnet.apiclient.APIClient;
import jnet.apiclient.APIUrl;
import org.sqlite.SQLiteException;

import static core.DataImporter.*;
/**
 * @author lilian battani
 * <p>Import pojemon egg groups</p>
 *
 */
public class EggGroupImporter extends DataImporter {
	
	/**
	 * 
	 * 
	 */
	public EggGroupImporter() {
		super();
	}
	
	/* (non-Javadoc)
	 * @see core.DataImporter#getData(core.PokeAPIConnection, core.PokeDBConnection)
	 */
	@Override
	public void getData(PokeAPIConnection api, PokeDBConnection db) {
		// TODO Auto-generated method stub
		try{
			APIClient pokeapi=api.getPokeApi();
			APIUrl pokeurl=pokeapi.getAPIUrl();
			Connection pokedb=db.getDB();
			//
			pokeurl.setHasFile(false); 
			pokeurl.addToPath("egg-group").addToPath("?offset=0&limit=20");
			pokeapi.updateConnection();
			//
			JsonObject e=pokeapi.get().getAsJsonObject();
			JsonArray groups=null;
			JsonObject eggGroup=null;
			APIUrl group_url=null;
			PreparedStatement request=null;
			String[] url_elements=null;
			String name=null;
			boolean notNull=true;
			int id=0;
			int i=0;

			while( notNull ){
				pokeurl.setHasFile(true);
				pokeurl.delFromPath();
				// 
				notNull=!(e.get("next").isJsonNull());
				groups=e.get("results").getAsJsonArray();
				//
				for( JsonElement jse : groups ){
					JsonObject jso=jse.getAsJsonObject();
					group_url=new APIUrl(jso.get("url").getAsString());
					//
					pokeurl.addToPath(group_url.getPath().getLast());
					pokeapi.updateConnection();

					eggGroup=pokeapi.get().getAsJsonObject();

					id=eggGroup.get("id").getAsInt();
					i=searchWithLanguage(eggGroup.get("names").getAsJsonArray());

					if( i != -1 )
						name=eggGroup.get("names").getAsJsonArray().get(i).getAsJsonObject().get("name").getAsString();
					else
						name=eggGroup.get("name").getAsString();

					// database insert
					try{
						request=pokedb.prepareStatement("insert into egg_group(id,name) values(?,?);");
						request.setInt(1,id);
						request.setString(2,name);
						request.executeUpdate();
					}catch(SQLiteException se){}
					//
					pokeurl.delFromPath();
				}
				
				pokeurl.setHasFile(false);

				if( notNull ){
					url_elements=e.get("next").getAsString().split("/");
					pokeurl.addToPath(url_elements[url_elements.length-1]);
					pokeapi.updateConnection();
					e=pokeapi.get().getAsJsonObject();
				}
			}

			pokedb.commit();
			// 
		}catch(Exception e){
			throw new RuntimeException(e);
		}finally{
			this.reset();
		}
	}

}
