/**
 * 
 */
package core;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLIntegrityConstraintViolationException;

import org.sqlite.SQLiteException;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonArray;

import jnet.apiclient.APIClient;
import jnet.apiclient.APIUrl;

import static core.DataImporter.*;

/**
 * @author lilian battani
 * <p>Import pokemon abilities</p>
 *
 */
public class AbilityImporter extends DataImporter {

	/**
	 *
	 *
	 */
	public AbilityImporter(){
		super();
	}

	/**
	 * @see core.DataImporter#getData(core.PokeAPIConnection, core.PokeDBConnection)
	 * @throws java.lang.RuntimeException
	 */
	@Override
	public void getData(PokeAPIConnection api, PokeDBConnection db) throws RuntimeException{
		try{
		// TODO Auto-generated method stub
			APIClient pokeapi=api.getPokeApi();
			APIUrl pokeurl=pokeapi.getAPIUrl();
			Connection pokedb=db.getDB();

			pokeurl.setHasFile(false);
			pokeurl.addToPath("ability").addToPath("?offset=0&limit=20");
			pokeapi.updateConnection();

			// initializations
			PreparedStatement request=null;
			JsonObject e=pokeapi.get().getAsJsonObject();
			JsonObject ability=null;
			JsonArray abilities=null;
			JsonArray names=null;
			JsonArray descriptions=null;
			String[] url_elements=null;
			APIUrl ability_url=null;
			String url=null;
			boolean notNull=true;
			int abilityId=0;
			String abilityName=null;
			String abilityDescription=null;
			int j=0;
			int k=0;

			while( notNull ){
				pokeurl.setHasFile(true);
				pokeurl.delFromPath();

				notNull=!(e.get("next").isJsonNull());
				abilities=e.get("results").getAsJsonArray();

				for( JsonElement jse : abilities ){
					// load the ability designated by i
					url=jse.getAsJsonObject().get("url").getAsString();
					ability_url=new APIUrl(url);

					pokeurl.addToPath(ability_url.getPath().getLast());
					pokeapi.updateConnection();
					ability=pokeapi.get().getAsJsonObject();
					//retrieve data from json 
					names=ability.get("names").getAsJsonArray();
					descriptions=ability.get("flavor_text_entries").getAsJsonArray();
					
					j=searchWithLanguage(names);
					k=searchWithLanguage(descriptions);

					// ability data 
					abilityId=ability.get("id").getAsInt();

					if( j != -1 )
						abilityName=names.get(j).getAsJsonObject().get("name").getAsString();
					else
						abilityName=ability.get("name").getAsString();

					if( k != -1 )
						abilityDescription=descriptions.get(k).getAsJsonObject().get("flavor_text").getAsString();
					else if( ability.get("effect_entries").getAsJsonArray().size() > 0 )
						abilityDescription=ability.get("effect_entries").getAsJsonArray().get(0).getAsJsonObject().get("effect").getAsString();
					else
						abilityDescription="";

					// insert data into the database

					try{
						request=pokedb.prepareStatement("insert into ability(id,name,description) values(?,?,?);");
						request.setInt(1,abilityId);
						request.setString(2,abilityName);
						request.setString(3,abilityDescription);
						request.executeUpdate();
					}catch(SQLiteException se){}
					//
					pokeurl.delFromPath();
				}

				pokeurl.setHasFile(false);

				if( notNull ){
					url_elements=e.get("next").getAsString().split("/");
					pokeurl.addToPath(url_elements[url_elements.length-1]);
					pokeapi.updateConnection();
					e=pokeapi.get().getAsJsonObject();
				}
			}

			// save changes
			pokedb.commit();

		}catch(Exception e){
			throw new RuntimeException(e);
		}finally{
			this.reset();
		}
	}

}
