/**
 * 
 */
package core;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;

import java.util.LinkedList;

/**
 * @author lilian battani
 * <p>DataImporter that run several DataImporters</p>
 *
 */
public class MultiImporter extends DataImporter {

	/**
	 * importers contained in this importer
	 *
	 */
	private LinkedList<DataImporter> importers;

	/**
	 *
	 */
	private SimpleBooleanProperty loading;

	/**
	 *
	 */
	private SimpleIntegerProperty loaded;

	/**
	 *
	 *
	 */
	public MultiImporter(){
		super();
		importers=new LinkedList<>();
		loading=new SimpleBooleanProperty();
		loaded=new SimpleIntegerProperty();

		loading.set(false);
		loaded.set(0);
	}

	/**
	 *
	 * @return number of importers contained in this importer
	 */
	public int importerCount(){
		return importers.size();
	}

	/**
	 *
	 * @return
	 */
	public boolean isLoading() {
		return loading.get();
	}

	/**
	 *
	 * @return
	 */
	public SimpleBooleanProperty loadingProperty() {
		return loading;
	}

	/**
	 *
	 * @return
	 */
	public int getLoaded() {
		return loaded.get();
	}

	/**
	 *
	 * @return
	 */
	public SimpleIntegerProperty loadedProperty() {
		return loaded;
	}

	/**
	 * add an importer
	 * @param im importer to add
	 * @throws java.lang.NullPointerException if im is null
	 *
	 */
	public void add(DataImporter im) throws NullPointerException{
		if( im == null )
			throw new NullPointerException("null");

		importers.addLast(im);
	}

	/**
	 * remove all the importers from this multi-importer
	 */
	public void reset(){
		importers.clear();
		loading.set(false);
		loaded.set(0);
	}

	/* (non-Javadoc)
	 * @see core.DataImporter#getData(core.PokeAPIConnection, core.PokeDBConnection)
	 */
	@Override
	public void getData(PokeAPIConnection api, PokeDBConnection db) {
		// TODO Auto-generated method stub
		loading.set(true);
		loaded.set(0);

		importers.stream().forEach( i -> {
			i.getData(api,db);
			loaded.set(loaded.get()+1);
		} );

		//
		loading.set(false);
		loaded.set(0);
	}

}
