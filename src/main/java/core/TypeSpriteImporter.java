/**
 * 
 */
package core;

import java.sql.Statement;

/**
 * @author lilian battani
 * <p>Import types sprites</p>
 */
public class TypeSpriteImporter extends DataImporter {

	/**
	 *
	 *
	 */
	public TypeSpriteImporter(){
		super();
	}

	/* (non-Javadoc)
	 * @see core.DataImporter#getData(core.PokeAPIConnection, core.PokeDBConnection)
	 */
	@Override
	public void getData(PokeAPIConnection api, PokeDBConnection db) {
		// TODO Auto-generated method stub
		try{
			int i = 1;
			String str = null;
			ImageWriter writer = new ImageWriter();
			//
			Statement s=db.getDB().createStatement();
			s.executeUpdate("update type set sprite='images/TYPE'||id||'.png';");
			//
			while( i <= 18 ){
				str = writer.writeImage(this.getClass().getClassLoader().getResourceAsStream("TYPE"+i+".png"),"TYPE"+i+".png");

				if( str == null ){
					db.getDB().rollback();
					return;
				}else
					i++;
			}
			//
			db.getDB().commit();
		}catch(Exception e){
			throw new RuntimeException(e);
		}
	}

}
