package core;

import gui.StageManager;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import model.*;

import java.sql.SQLException;
import java.util.function.Function;

/**
 * util methods for event handlers
 */
public class ListenerUtils {

    /**
     *
     * @param l
     */
    public static void addPokemonListListener(ListView<DBObject> l){
        addListListener(l,m -> {
            m.pokemon((Pokemon) l.getFocusModel().getFocusedItem());
            return null;
        });
    }

    /**
     *
     * @param l
     */
    public static void addAbilityListListener(ListView<DBObject> l){
        addListListener(l,m -> {
            m.ability((Ability) l.getFocusModel().getFocusedItem());
            return null;
        });
    }

    /**
     *
     * @param l
     */
    public static void addEggGroupListListener(ListView<DBObject> l){
        addListListener(l,m -> {
            m.eggGroup((EggGroup) l.getFocusModel().getFocusedItem());
            return null;
        });
    }

    /**
     *
     * @param l
     */
    public static void addMoveListListener(ListView<DBObject> l){
        addListListener(l,m -> {
            m.move((Move) l.getFocusModel().getFocusedItem());
            return null;
        });
    }

    /**
     *
     * @param l
     */
    public static void addTypeListListener(ListView<DBObject> l){
        addListListener(l,m -> {
            m.type((Type) l.getFocusModel().getFocusedItem());
            return null;
        });
    }

    /**
     *
     * @param iv
     * @param t1
     * @param t2
     */
    public static void addTypeImageListener(ImageView iv,Type t1,Type t2){
        iv.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if( doubleClick(mouseEvent) )
                    StageManager.get().type(t1,t2);
            }
        });
    }

    /**
     *
     * @param l
     * @param a
     */
    public static void addAbilityListener(Label l, Ability a){
        l.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if( mouseEvent.getClickCount() >= 2 && a != null )
                    StageManager.get().ability(a);
            }
        });
    }

    /**
     *
     * @param l
     * @param eg
     */
    public static void addEggGroupListener(Label l,EggGroup eg){
        l.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if( mouseEvent.getClickCount() >= 2 && eg != null )
                    StageManager.get().eggGroup(eg);
            }
        });
    }

    /**
     *
     * @param l
     */
    public static void addNotePadListListener(ListView<DBObject> l){
        l.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if( doubleClick(mouseEvent) && ! l.getItems().isEmpty() )
                    StageManager.get().notepad((NotePad)l.getFocusModel().getFocusedItem());
            }
        });


        l.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if( keyEvent.isControlDown() && keyEvent.getCode() == KeyCode.N){
                    StageManager.get().notepad();
                }else if(keyEvent.isControlDown() && keyEvent.getCode() == KeyCode.D){
                    NotePad n = (NotePad)l.getFocusModel().getFocusedItem();

                    try {
                        n.delete();
                    }catch (SQLException sqle){}

                    l.getSelectionModel().clearSelection();
                    l.getItems().remove(n);
                }
            }
        });
    }

    /**
     *
     * @param m
     * @return
     */
    public static boolean doubleClick(MouseEvent m){
        return m.getClickCount() == 2;
    }

    /**
     *
     * @param m
     * @return
     */
    public static boolean tripleClick(MouseEvent m){
        return m.getClickCount() == 3;
    }

    /**
     *
     * @param m
     * @return
     */
    public static boolean rightClick(MouseEvent m){
        return m.isSecondaryButtonDown();
    }

    /**
     *
     * @param l
     * @param action
     */
    private static void addListListener(ListView<DBObject> l, Function<StageManager,Void> action){
        l.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if( doubleClick(mouseEvent) && ! l.getItems().isEmpty() )
                    action.apply(StageManager.get());
            }
        });
    }
}
