/**
 * 
 */
package core;

import java.util.List;

import java.util.stream.Collectors;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLIntegrityConstraintViolationException;

import com.google.gson.JsonElement;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import jnet.apiclient.APIClient;
import jnet.apiclient.APIUrl;
import org.sqlite.SQLiteException;

import static core.DataImporter.*;

/**
 * @author lilian battani
 * <p>Import pokemon types(eg:water,grass,etc...)</p>
 *
 */
public class TypeImporter extends DataImporter {

	/**
	 *
	 *
	 */
	public TypeImporter(){
		super();
	}

	/* (non-Javadoc)
	 * @see core.DataImporter#getData(core.PokeAPIConnection, core.PokeDBConnection)
	 */
	@Override
	public void getData(PokeAPIConnection api, PokeDBConnection db) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		try{
			APIClient pokeapi=api.getPokeApi();
			APIUrl pokeurl=pokeapi.getAPIUrl();
			Connection pokedb=db.getDB();
			//
			pokeurl.setHasFile(false); 
			pokeurl.addToPath("type").addToPath("?offset=0&limit=20");
			pokeapi.updateConnection();
			//
			JsonObject e=pokeapi.get().getAsJsonObject();
			JsonArray types=null;
			JsonObject type=null;
			JsonObject jso=null;
			APIUrl type_url=null;
			PreparedStatement request=null;
			String[] url_elements=null;
			String name=null;
			boolean notNull=true;
			int id=0;
			int i=0;
			// type table
			JsonObject typeTable=new JsonObject();
			JsonObject typeRelations=null;

			while( notNull ){
				pokeurl.setHasFile(true);
				pokeurl.delFromPath();
				// 
				notNull=!(e.get("next").isJsonNull());
				types=e.get("results").getAsJsonArray();
				//
				for( JsonElement jse : types ){
					jso=jse.getAsJsonObject();
					type_url=new APIUrl(jso.get("url").getAsString());

					pokeurl.addToPath(type_url.getPath().getLast());
					pokeapi.updateConnection();

					type=pokeapi.get().getAsJsonObject();

					if( type.get("pokemon").getAsJsonArray().size() > 0 && type.get("moves").getAsJsonArray().size() > 0 ){
						typeRelations=new JsonObject();
						// 
						id=type.get("id").getAsInt();
						typeTable.add((new Integer(id)).toString(),typeRelations);

						i=searchWithLanguage(type.get("names").getAsJsonArray());

						if( i != -1 )
							name=type.get("names").getAsJsonArray().get(i).getAsJsonObject().get("name").getAsString();
						else
							name=type.get("name").getAsString();

						// retrieving type affinities
						for( JsonElement typeElement : type.get("damage_relations").getAsJsonObject().get("double_damage_to").getAsJsonArray() ){
							type_url=new APIUrl(typeElement.getAsJsonObject().get("url").getAsString());
							typeRelations.add(type_url.getPath().getLast(),new JsonPrimitive(2.0));
						}

						for( JsonElement typeElement : type.get("damage_relations").getAsJsonObject().get("half_damage_to").getAsJsonArray() ){
							type_url=new APIUrl(typeElement.getAsJsonObject().get("url").getAsString());
							typeRelations.add(type_url.getPath().getLast(),new JsonPrimitive(0.5));
						}

						for( JsonElement typeElement : type.get("damage_relations").getAsJsonObject().get("no_damage_to").getAsJsonArray() ){
							type_url=new APIUrl(typeElement.getAsJsonObject().get("url").getAsString());
							typeRelations.add(type_url.getPath().getLast(),new JsonPrimitive(0.0));
						}


						// database insert
						try{
							request=pokedb.prepareStatement("insert into type(id,name) values(?,?);");
							request.setInt(1,id);
							request.setString(2,name);
							request.executeUpdate();
						}catch(SQLiteException se){}
					}
					//
					pokeurl.delFromPath();
				}
				
				pokeurl.setHasFile(false);

				if( notNull ){
					url_elements=e.get("next").getAsString().split("/");
					pokeurl.addToPath(url_elements[url_elements.length-1]);
					pokeapi.updateConnection();
					e=pokeapi.get().getAsJsonObject();
				}
			}

			pokedb.commit();
			// fill type table
			List<String> typeList=typeTable.entrySet().stream().map( (t)-> t.getKey() ).collect(Collectors.toList());
			double damageMultiplier=0.0;

			for( String t_attacker : typeList){
				typeRelations=typeTable.get(t_attacker).getAsJsonObject();

				for( String t_defender : typeList){
					damageMultiplier=typeRelations.has(t_defender)?typeRelations.get(t_defender).getAsDouble():1.0;
					
					try{
						request=pokedb.prepareStatement("insert into type_table(attacker_id,defender_id,damage_multiplier) values(?,?,?)");
						request.setInt(1,Integer.parseInt(t_attacker));
						request.setInt(2,Integer.parseInt(t_defender));
						request.setDouble(3,damageMultiplier);
						request.executeUpdate();
					}catch(SQLiteException se){}
				}
			}

			pokedb.commit();
		}catch(Exception e){
			throw new RuntimeException(e);
		}finally{
			this.reset();
		}
	}

}
