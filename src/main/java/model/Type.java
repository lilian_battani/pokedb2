/**
 * 
 */
package model;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

import java.io.IOException;

import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import core.PokeDBConnection;

import static model.DBObject.*;

/**
 * @author lilian battani
 * <p>Pokemon Types</p>
 */
public class Type extends DBObject implements HaveMoves,HavePokemons{

	/**
	 * immune constant
	 *
	 */
	public final static double IMMUNE=0;

	/**
	 * double-resist constant
	 *
	 */
	public final static double DOUBLE_RESISTS=0.25;

	/**
	 * resists constant
	 *
	 */
	public final static double RESISTS=0.5;

	/**
	 * neutral constant
	 *
	 */
	public final static double NEUTRAL=1;

	/**
	 * super efficient constant
	 *
	 */
	public final static double SUPER_EFFICIENT=2;

	/**
	 * double super efficient constant
	 *
	 */
	public final static double DOUBLE_SUPER_EFFICIENT=4;

	/**
	 * type sprite
	 *
	 */
	private String sprite;

	/**
	 * create a new type
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 *
	 */
	private Type() throws SQLException,IOException,ClassNotFoundException{
		super();
	}

	/**
	 * @return the sprite
	 */
	public String getSprite() {
		return sprite;
	}

	/**
	 * @return affinities of this type from the other types
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 *
	 */
	public Map<Type,Double> getAffinitiesFrom() throws SQLException,IOException,ClassNotFoundException{
		synchronized(this.getDb()){
			Map<Type,Double> affinities=new HashMap<>();
			ResultSet result=null;
			Type t=null;
			boolean hasNext=true;

			PreparedStatement request=this.getDb().prepareStatement(
				"select attacker.id as id,attacker.name as name,attacker.sprite as sprite,damage_multiplier"+
				"	from type attacker,type defender,type_table t"+
				"	where attacker.id = t.attacker_id"+
				"	and defender.id = t.defender_id"+
				"	and defender.id = ?"
			);

			request.setInt(1,this.getId());

			result=request.executeQuery();

			while( hasNext ){
				t=findType(result,result.next());
				hasNext=(t!=null);

				if( hasNext )
					affinities.put(t,result.getDouble("damage_multiplier"));
			}

			return affinities;
		}
	}

	/**
	 * @return affinities of this type to the other types
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 *
	 */
	public Map<Type,Double> getAffinitiesTo() throws SQLException,IOException,ClassNotFoundException{
		synchronized(this.getDb()){
			Map<Type,Double> affinities=new HashMap<>();
			ResultSet result=null;
			Type t=null;
			boolean hasNext=true;

			PreparedStatement request=this.getDb().prepareStatement(
				"select defender.id as id,defender.name as name,defender.sprite as sprite,damage_multiplier"+
				"	from type attacker,type defender,type_table t"+
				"	where attacker.id = t.attacker_id"+
				"	and defender.id = t.defender_id"+
				"	and attacker.id = ?"
			);

			request.setInt(1,this.getId());

			result=request.executeQuery();

			while( hasNext ){
				t=findType(result,result.next());
				hasNext=(t!=null);

				if( hasNext )
					affinities.put(t,result.getDouble("damage_multiplier"));
			}

			return affinities;
		}
	}

	/**
	 * @see model.HaveMoves#getMoves()
	 *
	 */
	public List<Move> getMoves()  throws IOException,SQLException,ClassNotFoundException{
		synchronized(this.getDb()){
			PreparedStatement request=this.getDb().prepareStatement(
				"select * from move\n"+
				"	where type=?;\n"
			);

			request.setInt(1,this.getId());

			return Move.findMoves(request.executeQuery());
		}
	}

	/**
	 * @see model.HavePokemons#getPokemons()
	 *
	 */
	public List<Pokemon> getPokemons() throws IOException,SQLException,ClassNotFoundException{
		synchronized(this.getDb()){
			PreparedStatement request=this.getDb().prepareStatement(
				"select * from pokemon\n"+
				"	where first_type=?\n"+
				"	or second_type=?;\n"
			);

			request.setInt(1,this.getId());
			request.setInt(2,this.getId());

			return Pokemon.findPokemons(request.executeQuery());
		}
	}

	/**
	 * @param sprite the sprite to set
	 * @throws java.lang.NullPointerException if sprite is null
	 */
	public void setSprite(String sprite) {
		if( sprite == null )
			throw new NullPointerException("null");

		this.sprite = sprite;
	}

	/**
	 * @throws java.lang.IllegalStateException if the id is NEW_ID
	 * @see model.DBObject#save()
	 */
	@Override
	public void save() throws SQLException {
		// TODO Auto-generated method stub
		if( getId() == NEW_ID )
			throw new IllegalStateException(getId()+" not in type");

		synchronized(this.getDb()){
			PreparedStatement request=this.getDb().prepareStatement(
				"update type\n"+
				"	set name=?,\n"+
				"	sprite=?\n"+ 
				"where id=?;\n"
			);

			request.setString(1,this.getName());
			request.setString(2,this.sprite);
			request.setInt(3,this.getId());

			request.executeUpdate();
			// 
			this.getDb().commit();
		}
	}

	/* (non-Javadoc)
	 * @see model.DBObject#delete()
	 */
	@Override
	public void delete() throws SQLException {
		// TODO Auto-generated method stub
		synchronized(this.getDb()){
			PreparedStatement request=this.getDb().prepareStatement("delete from type where id=?;");

			request.setInt(1,this.getId());
			request.executeUpdate();

			// 
			this.getDb().commit();
		}
	}

	/**
	 * find an type group with its id
	 * @param id id of the ability to find
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 *
	 */
	public static Type findById(int id) throws SQLException,IOException,ClassNotFoundException{
		synchronized(PokeDBConnection.getINSTANCE().getDB()){
			PreparedStatement request=PokeDBConnection.getINSTANCE().getDB().prepareStatement("select * from type where id=?;");
			request.setInt(1,id);

			ResultSet rs=request.executeQuery();

			return findType(rs,rs.next());
		}
	}

	/**
	 * find a type with his name
	 * @param n name of the egg group to find
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 *
	 */
	public static Type findByName(String n) throws SQLException,IOException,ClassNotFoundException{
		synchronized(PokeDBConnection.getINSTANCE().getDB()){
			PreparedStatement request=PokeDBConnection.getINSTANCE().getDB().prepareStatement("select * from type where lower(name)=?;");
			request.setString(1,n.toLowerCase());

			ResultSet rs=request.executeQuery();

			return findType(rs,rs.next());
		}
	}

	/**
	 * find all types
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 *
	 */
	public static List<Type> findAll() throws SQLException,IOException,ClassNotFoundException{
		synchronized(PokeDBConnection.getINSTANCE().getDB()){
			Statement request=PokeDBConnection.getINSTANCE().getDB().createStatement();

			return findTypes(request.executeQuery("select * from type;"));
		}
	}

	/**
	 * find types with n String contained in his name
	 * @param n the String to find in type name
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 *
	 */
	public static List<Type> findByNameLike(String n) throws SQLException,IOException,ClassNotFoundException{
		synchronized(PokeDBConnection.getINSTANCE().getDB()){
			PreparedStatement request=PokeDBConnection.getINSTANCE().getDB().prepareStatement("select * from type where lower(name) like ?");
			request.setString(1,'%'+n.toLowerCase()+'%');

			return findTypes(request.executeQuery());
		}
	}

	/**
	 * @return the type table ( from type t1 (attacker) to type t2 (defender) )
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 *
	 */
	public static Map<Type,Map<Type,Double>> getTypeTable() throws SQLException,IOException,ClassNotFoundException{
		synchronized(PokeDBConnection.getINSTANCE().getDB()){
			Map<Type,Map<Type,Double>> typeTable=new HashMap<>();
			Map<Type,Double> typeAffinities=null;
			Type attacker=null;
			Type defender=null;

			Statement request=PokeDBConnection.getINSTANCE().getDB().createStatement();
			ResultSet result=request.executeQuery(
				"select attacker.id as attacker_id,attacker.name as attacker_name,attacker.sprite as attacker_sprite,defender.id as defender_id,defender.name as defender_name,defender.sprite as defender_sprite,tt.damage_multiplier as damage\n"+
				"from type attacker,type defender,type_table tt\n"+
				"	where attacker.id=tt.attacker_id\n"+
				"	and defender.id=tt.defender_id;\n"
			);

			while(result.next()){
				attacker=new Type();
				attacker.setId(result.getInt("attacker_id"));
				attacker.setName(result.getString("attacker_name"));
				attacker.setSprite(result.getString("attacker_sprite"));

				if( typeTable.get(attacker) == null ){
					typeAffinities=new HashMap<>();
					typeTable.put(attacker,typeAffinities);
				}else
					typeAffinities=typeTable.get(attacker);

				defender=new Type();
				defender.setId(result.getInt("defender_id"));
				defender.setName(result.getString("defender_name"));
				defender.setSprite(result.getString("defender_sprite"));

				typeAffinities.put(defender,result.getDouble("damage"));
			}

			return typeTable;
		}
	}

	/**
	 * merge two type affinities ( usefull for dual types )
	 * @param a1 first type affinities
	 * @param a2 second type affinities
	 * @throws java.lang.NullPointerException if a1 is null
	 *
	 */
	public static Map<Type,Double> mergeAffinities(Map<Type,Double> a1,Map<Type,Double> a2) throws NullPointerException{
		if( a1 == null )
			throw new NullPointerException("null");

		if( a2 == null )
			return new HashMap<Type,Double>(a1);

		Map<Type,Double> merged=new HashMap<>();

		a1.keySet().stream().forEach(
			(t) -> {
				merged.put(t,a1.get(t)*a2.get(t));
			}
		);

		return merged;
	}

	/**
	 * @param result the resultset
	 * @param next if result have data inside of it
	 * @return Type object from resultset
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 *
	 */
	private static Type findType(ResultSet result,boolean next) throws SQLException,IOException,ClassNotFoundException{
		if( ! next )
			return null;

		Type t=new Type();

		t.setId(result.getInt("id"));
		t.setName(result.getString("name"));
		t.setSprite(result.getString("sprite"));

		return t;
	}

	/**
	 * @param result the resultset
	 * @return Type objects from resultset
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 *
	 */
	static List<Type> findTypes(ResultSet result) throws SQLException,IOException,ClassNotFoundException{
		List<Type> al=new ArrayList<>();
		Type t=null;
		boolean haveNext=true;

		while( haveNext ){
			t=findType(result,result.next());
			haveNext=(t!=null);

			if( haveNext )
				al.add(t);
		}

		return al;
	}

}
