package model;

import core.PokeDBConnection;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class NotePad extends DBObject{

    /**
     *
     */
    private String content;

    /**
     * create a new DB object set it's id to NEW_ID
     *
     * @throws IOException
     * @throws SQLException
     * @throws ClassNotFoundException if mysql driver is not found
     */
    public NotePad() throws SQLException, IOException, ClassNotFoundException {
        super();
    }

    /**
     *
     * @return
     */
    public String getContent() {
        return content;
    }

    /**
     *
     * @param content
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     *
     * @throws SQLException
     */
    @Override
    public void save() throws SQLException {
        if( getId() == NEW_ID ){
            synchronized (this.getDb()) {

                PreparedStatement request = this.getDb().prepareStatement(
                        "insert into notepad(name,description)\n" +
                                "values (?,?);"
                );

                request.setString(1, this.getName());
                request.setString(2, this.content);

                request.executeUpdate();
                //
                this.getDb().commit();
            }
        }else {
            synchronized (this.getDb()) {

                PreparedStatement request = this.getDb().prepareStatement(
                        "update notepad\n" +
                                "	set name=?,\n" +
                                "	description=?\n" +
                                "where id=?;\n"
                );

                request.setString(1, this.getName());
                request.setString(2, this.content);
                request.setInt(3, this.getId());

                request.executeUpdate();
                //
                this.getDb().commit();
            }
        }
    }

    /**
     *
     * @throws SQLException
     */
    @Override
    public void delete() throws SQLException {
        // TODO Auto-generated method stub
        synchronized(this.getDb()){
            PreparedStatement request=this.getDb().prepareStatement("delete from notepad where id=?;");

            request.setInt(1,this.getId());
            request.executeUpdate();

            //
            this.getDb().commit();
        }
    }

    /**
     * find an ability with its id
     * @param id id of the ability to find
     * @throws java.io.IOException
     * @throws java.sql.SQLException
     * @throws java.lang.ClassNotFoundException if mysql driver is not found
     *
     */
    public static NotePad findById(int id) throws SQLException,IOException,ClassNotFoundException{
        synchronized(PokeDBConnection.getINSTANCE().getDB()){
            PreparedStatement request=PokeDBConnection.getINSTANCE().getDB().prepareStatement("select * from notepad where id=?;");
            request.setInt(1,id);

            ResultSet rs=request.executeQuery();

            return findNotePad(rs,rs.next());
        }
    }

    /**
     * find an ability with his name
     * @param n name of the ability to find
     * @throws java.io.IOException
     * @throws java.sql.SQLException
     * @throws java.lang.ClassNotFoundException if mysql driver is not found
     *
     */
    public static NotePad findByName(String n) throws SQLException,IOException,ClassNotFoundException{
        synchronized(PokeDBConnection.getINSTANCE().getDB()){
            PreparedStatement request=PokeDBConnection.getINSTANCE().getDB().prepareStatement("select * from notepad where lower(name)=?;");
            request.setString(1,n.toLowerCase());

            ResultSet rs=request.executeQuery();

            return findNotePad(rs,rs.next());
        }
    }

    /**
     * find all abilities
     * @throws java.io.IOException
     * @throws java.sql.SQLException
     * @throws java.lang.ClassNotFoundException if mysql driver is not found
     *
     */
    public static List<NotePad> findAll() throws SQLException,IOException,ClassNotFoundException{
        synchronized(PokeDBConnection.getINSTANCE().getDB()){
            Statement request=PokeDBConnection.getINSTANCE().getDB().createStatement();

            return findNotePads(request.executeQuery("select * from notepad;"));
        }
    }

    /**
     * find abilities with n String contained in his name
     * @param n the String to find in ability name
     * @throws java.io.IOException
     * @throws java.sql.SQLException
     * @throws java.lang.ClassNotFoundException if mysql driver is not found
     *
     */
    public static List<NotePad> findByNameLike(String n) throws SQLException,IOException,ClassNotFoundException{
        synchronized(PokeDBConnection.getINSTANCE().getDB()){
            PreparedStatement request=PokeDBConnection.getINSTANCE().getDB().prepareStatement("select * from notepad where lower(name) like ?");
            request.setString(1,'%'+n.toLowerCase()+'%');

            return findNotePads(request.executeQuery());
        }
    }

    /**
     * @param result the resultset
     * @param next if result have data inside of it
     * @return NotePad object from resultset
     * @throws java.io.IOException
     * @throws java.sql.SQLException
     * @throws java.lang.ClassNotFoundException if mysql driver is not found
     *
     */
    static NotePad findNotePad(ResultSet result,boolean next) throws SQLException,IOException,ClassNotFoundException{
        if( ! next )
            return null;

        NotePad n = new NotePad();

        n.setId(result.getInt("id"));
        n.setName(result.getString("name"));
        n.setContent(result.getString("description"));

        return n;
    }

    /**
     * @param result the resultset
     * @return Ability objects from resultset
     * @throws java.io.IOException
     * @throws java.sql.SQLException
     * @throws java.lang.ClassNotFoundException if mysql driver is not found
     *
     */
    static List<NotePad> findNotePads(ResultSet result) throws SQLException,IOException,ClassNotFoundException{
        List<NotePad> al=new ArrayList<>();
        NotePad a=null;
        boolean haveNext=true;

        while( haveNext ){
            a=findNotePad(result,result.next());
            haveNext=(a!=null);

            if( haveNext )
                al.add(a);
        }

        return al;
    }
}
