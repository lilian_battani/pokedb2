/**
 * 
 */
package model;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

import java.io.IOException;

import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;

import core.PokeDBConnection;

import static model.DBObject.*;

/**
 * @author lilian battani
 * <p>Pokemons</p>
 */
public class Pokemon extends DBObject implements HaveMoves {

	/**
	 * pokemon hp base stat
	 *
	 */
	private int hp;

	/**
	 * pokemon attack base stat
	 *
	 */
	private int attack;

	/**
	 * pokemon defense base stat
	 *
	 */
	private int defense;

	/** 
	 * pokemon special attack base stat
	 *
	 */
	private int speAtk;

	/**
	 * pokemon special defense base stat
	 *
	 */
	private int speDef;

	/**
	 * pokemon speed base stat
	 *
	 */
	private int speed;

	/**
	 * first pokemon type
	 *
	 */
	private int firstType;

	/**
	 * second pokemon type
	 *
	 */
	private int secondType;

	/**
	 * first pokemon egg group
	 *
	 */
	private int firstEggGroup;

	/**
	 * second pokemon egg group
	 *
	 */
	private int secondEggGroup;

	/**
	 * pokemon description
	 *
	 */
	private String description;

	/**
	 * pokemon sprite file
	 *
	 */
	private String sprite;

	/**
	 * create a new pokemon
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 *
	 */
	private Pokemon() throws IOException,SQLException,ClassNotFoundException{
		super();
	}

	/**
	 * @return the hp
	 */
	public int getHp() {
		return hp;
	}

	/**
	 * @return the attack
	 */
	public int getAttack() {
		return attack;
	}

	/**
	 * @return the defense
	 */
	public int getDefense() {
		return defense;
	}

	/**
	 * @return the speAtk
	 */
	public int getSpeAtk() {
		return speAtk;
	}

	/**
	 * @return the speDef
	 */
	public int getSpeDef() {
		return speDef;
	}

	/**
	 * @return the speed
	 */
	public int getSpeed() {
		return speed;
	}

	/**
	 * @return the total of pokemon stats
	 *
	 */
	public int getBaseStat(){
		return hp+attack+defense+speAtk+speDef+speed;
	}

	/**
	 * @return the firstType
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 */
	public Type getFirstType() throws IOException,SQLException,ClassNotFoundException{
		return Type.findById(firstType);
	}

	/**
	 * @return the secondType
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 */
	public Type getSecondType() throws IOException,SQLException,ClassNotFoundException{
		return Type.findById(secondType);
	}

	/**
	 * @return the firstEggGroup
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 */
	public EggGroup getFirstEggGroup() throws IOException,SQLException,ClassNotFoundException{
		return EggGroup.findById(firstEggGroup);
	}

	/**
	 * @return the secondEggGroup
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 */
	public EggGroup getSecondEggGroup() throws IOException,SQLException,ClassNotFoundException{
		return EggGroup.findById(secondEggGroup);
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @return the sprite
	 */
	public String getSprite() {
		return sprite;
	}

	/**
	 * @return pokemon abilities ( the boolean indicates if the ability is hidden or not )
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 *
	 */
	public Map<Ability,Boolean> getAbilities() throws IOException,SQLException,ClassNotFoundException{
		synchronized(this.getDb()){
			Map<Ability,Boolean> abilities=new HashMap<>();
			Ability a=null;
			boolean haveNext=true;

			PreparedStatement request=this.getDb().prepareStatement(
				"select a.id as id,a.name as name,a.description as description,ha.hidden as hidden\n"+
				"	from ability a,have_ability ha\n"+
				"	where a.id=ha.ability\n"+
				"	and pokemon=?\n"+
				"	order by ha.hidden;\n"
			);

			request.setInt(1,this.getId());

			ResultSet result=request.executeQuery();

			while( haveNext ){
				a=Ability.findAbility(result,result.next());
				haveNext=(a!=null);

				if( haveNext )
					abilities.put(a,result.getBoolean("hidden"));
			}

			return abilities;
		}
	}

	/**
	 * @return pokemon evolutions
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 *
	 */
	public List<Pokemon> getEvolutions() throws IOException,SQLException,ClassNotFoundException{
		synchronized(this.getDb()){
			PreparedStatement request=this.getDb().prepareStatement(
				"select * from pokemon,evolve\n"+
				"	where evolve.evolve_into=pokemon.id\n"+
				"	and evolve.evolve_from=?;\n"
			);

			request.setInt(1,this.getId());
			
			return findPokemons(request.executeQuery());
		}

	}

	/**
	 * @return pokemon level moves
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 *
	 */
	public Map<Move,Integer> getLevelMoves() throws IOException,SQLException,ClassNotFoundException{
		synchronized(this.getDb()){
			Map<Move,Integer> levelMoves=new HashMap<>();
			Move m=null;
			boolean haveNext=true;

			PreparedStatement request=this.getDb().prepareStatement(
				"select * from move,pokemon_move,level_move\n"+
				"	where move.id=pokemon_move.move\n"+
				"	and pokemon_move.pokemon=?\n"+
				"	and pokemon_move.pokemon=level_move.pokemon\n"+
				"	and pokemon_move.move=level_move.move\n"+
				"order by lvl asc;\n"
			);

			request.setInt(1,this.getId());

			ResultSet result=request.executeQuery();

			while( haveNext ){
				m=Move.findMove(result,result.next());
				haveNext=(m!=null);

				if( haveNext )
					levelMoves.put(m,result.getInt("lvl"));
			}

			return levelMoves;
		}
	}

	/**
	 * @return pokemon egg moves
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 *
	 */
	public List<Move> getEggMoves() throws IOException,SQLException,ClassNotFoundException{
		synchronized(this.getDb()){
			PreparedStatement request=this.getDb().prepareStatement(
				"select * from move,pokemon_move,egg_move\n"+
				"	where move.id=pokemon_move.move\n"+
				"	and pokemon_move.pokemon=?\n"+
				"	and pokemon_move.pokemon=egg_move.pokemon\n"+
				"	and pokemon_move.move=egg_move.move\n"
			);

			request.setInt(1,this.getId());

			return Move.findMoves(request.executeQuery());
		}
	}

	/**
	 * @return pokemon tutor moves
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 *
	 */
	public List<Move> getTutorMoves() throws IOException,SQLException,ClassNotFoundException{
		synchronized(this.getDb()){
			PreparedStatement request=this.getDb().prepareStatement(
				"select * from move,pokemon_move,tutor_move\n"+
				"	where move.id=pokemon_move.move\n"+
				"	and pokemon_move.pokemon=?\n"+
				"	and pokemon_move.pokemon=tutor_move.pokemon\n"+
				"	and pokemon_move.move=tutor_move.move\n"
			);

			request.setInt(1,this.getId());

			return Move.findMoves(request.executeQuery());
		}
	}

	/**
	 * @return pokemon ct moves
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 *
	 */
	public List<Move> getCtMoves() throws IOException,SQLException,ClassNotFoundException{
		synchronized(this.getDb()){
			PreparedStatement request=this.getDb().prepareStatement(
				"select * from move,pokemon_move,ct_move\n"+
				"	where move.id=pokemon_move.move\n"+
				"	and pokemon_move.pokemon=?\n"+
				"	and pokemon_move.pokemon=ct_move.pokemon\n"+
				"	and pokemon_move.move=ct_move.move\n"
			);

			request.setInt(1,this.getId());

			return Move.findMoves(request.executeQuery());
		}
	}

	/**
	 * @return pokemon dt moves
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 *
	 */
	public List<Move> getDtMoves() throws IOException,SQLException,ClassNotFoundException{
		synchronized(this.getDb()){
			PreparedStatement request=this.getDb().prepareStatement(
				"select * from move,pokemon_move,dt_move\n"+
				"	where move.id=pokemon_move.move\n"+
				"	and pokemon_move.pokemon=?\n"+
				"	and pokemon_move.pokemon=dt_move.pokemon\n"+
				"	and pokemon_move.move=dt_move.move\n"
			);

			request.setInt(1,this.getId());

			return Move.findMoves(request.executeQuery());
		}
	}

	/**
	 * @return pokemon event moves
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 *
	 */
	public List<Move> getEventMoves() throws IOException,SQLException,ClassNotFoundException{
		synchronized(this.getDb()){
			PreparedStatement request=this.getDb().prepareStatement(
				"select * from move,pokemon_move,event_move\n"+
				"	where move.id=pokemon_move.move\n"+
				"	and pokemon_move.pokemon=?\n"+
				"	and pokemon_move.pokemon=event_move.pokemon\n"+
				"	and pokemon_move.move=event_move.move\n"
			);

			request.setInt(1,this.getId());

			return Move.findMoves(request.executeQuery());
		}
	}

	/**
	 * @see model.HaveMoves#getMoves()
	 *
	 */
	public List<Move> getMoves()  throws IOException,SQLException,ClassNotFoundException{
		synchronized(this.getDb()){
			PreparedStatement request=this.getDb().prepareStatement(
				"select * from move,pokemon_move\n"+
				"	where move.id=pokemon_move.move\n"+
				"	and pokemon_move.pokemon=?;\n"
			);

			request.setInt(1,this.getId());

			return Move.findMoves(request.executeQuery());
		}
	}

	/**
	 * @param hp the hp to set
	 */
	public void setHp(int hp) {
		this.hp = hp;
	}

	/**
	 * @param attack the attack to set
	 */
	public void setAttack(int attack) {
		this.attack = attack;
	}

	/**
	 * @param defense the defense to set
	 */
	public void setDefense(int defense) {
		this.defense = defense;
	}

	/**
	 * @param speAtk the speAtk to set
	 */
	public void setSpeAtk(int speAtk) {
		this.speAtk = speAtk;
	}

	/**
	 * @param speDef the speDef to set
	 */
	public void setSpeDef(int speDef) {
		this.speDef = speDef;
	}

	/**
	 * @param speed the speed to set
	 */
	public void setSpeed(int speed) {
		this.speed = speed;
	}

	/**
	 * @param firstType the firstType to set
	 */
	public void setFirstType(int firstType) {
		this.firstType = firstType;
	}

	/**
	 * @param secondType the secondType to set
	 */
	public void setSecondType(int secondType) {
		this.secondType = secondType;
	}

	/**
	 * @param firstEggGroup the firstEggGroup to set
	 */
	public void setFirstEggGroup(int firstEggGroup) {
		this.firstEggGroup = firstEggGroup;
	}

	/**
	 * @param secondEggGroup the secondEggGroup to set
	 */
	public void setSecondEggGroup(int secondEggGroup) {
		this.secondEggGroup = secondEggGroup;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @param sprite the sprite to set
	 */
	public void setSprite(String sprite) {
		this.sprite = sprite;
	}

	/**
	 * @throws java.lang.IllegalStateException if the id is NEW_ID
	 * @see model.DBObject#save()
	 */
	@Override
	public void save() throws SQLException {
		// TODO Auto-generated method stub
		if( getId() == NEW_ID )
			throw new IllegalStateException(getId()+" not in pokemon");

		synchronized(this.getDb()){
			PreparedStatement request=this.getDb().prepareStatement(
				"update pokemon\n"+
				"	set name=?,\n"+
				"	base_pv=?,\n"+
				"	base_atk=?,\n"+
				"	base_def=?,\n"+
				"	base_spe_atk=?,\n"+
				"	base_spe_def=?,\n"+
				"	base_spd=?,\n"+
				"	first_type=?,\n"+
				"	second_type=?,\n"+
				"	first_egg_group=?,\n"+
				"	second_egg_group=?,\n"+
				"	sprite=?,\n"+
				"	description=?\n"+
				"where id=?;\n"
			);

			request.setString(1,this.getName());
			request.setInt(2,this.getHp());
			request.setInt(3,this.getAttack());
			request.setInt(4,this.getDefense());
			request.setInt(5,this.getSpeAtk());
			request.setInt(6,this.getSpeDef());
			request.setInt(7,this.getSpeed());

			request.setInt(8,this.firstType);
		
			if( this.secondType == NO_ID )
				request.setNull(9,Types.INTEGER);
			else
				request.setInt(9,this.secondType);

			request.setInt(10,this.firstEggGroup);

			if( this.secondType == NO_ID )
				request.setNull(11,Types.INTEGER);
			else
				request.setInt(11,this.secondEggGroup);

			if(  this.sprite == null )
				request.setNull(12,Types.VARCHAR);
			else
				request.setString(12,this.getSprite());

			request.setString(13,this.getDescription());

			request.setInt(14,this.getId());

			request.executeUpdate();
			// 
			this.getDb().commit();
		}
	}

	/* (non-Javadoc)
	 * @see model.DBObject#delete()
	 */
	@Override
	public void delete() throws SQLException {
		// TODO Auto-generated method stub
		synchronized(this.getDb()){
			PreparedStatement request=this.getDb().prepareStatement("delete from pokemon where id=?;");

			request.setInt(1,this.getId());
			request.executeUpdate();

			// 
			this.getDb().commit();
		}
	}

	/**
	 * find an ability with its id
	 * @param id id of the ability to find
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 *
	 */
	public static Pokemon findById(int id) throws SQLException,IOException,ClassNotFoundException{
		synchronized(PokeDBConnection.getINSTANCE().getDB()){
			PreparedStatement request=PokeDBConnection.getINSTANCE().getDB().prepareStatement("select * from pokemon where id=?;");
			request.setInt(1,id);

			ResultSet rs=request.executeQuery();

			return findPokemon(rs,rs.next());
		}
	}

	/**
	 * find an ability with his name
	 * @param n name of the ability to find
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 *
	 */
	public static Pokemon findByName(String n) throws SQLException,IOException,ClassNotFoundException{
		synchronized(PokeDBConnection.getINSTANCE().getDB()){
			PreparedStatement request=PokeDBConnection.getINSTANCE().getDB().prepareStatement("select * from pokemon where lower(name)=?;");
			request.setString(1,n.toLowerCase());

			ResultSet rs=request.executeQuery();

			return findPokemon(rs,rs.next());
		}
	}

	/**
	 * find all pokemons
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 *
	 */
	public static List<Pokemon> findAll() throws SQLException,IOException,ClassNotFoundException{
		synchronized(PokeDBConnection.getINSTANCE().getDB()){
			Statement request=PokeDBConnection.getINSTANCE().getDB().createStatement();

			return findPokemons(request.executeQuery("select * from pokemon;"));
		}
	}

	/**
	 * find pokemons with n String contained in his name
	 * @param n the String to find in pokemon name
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 *
	 */
	public static List<Pokemon> findByNameLike(String n) throws SQLException,IOException,ClassNotFoundException{
		synchronized(PokeDBConnection.getINSTANCE().getDB()){
			PreparedStatement request=PokeDBConnection.getINSTANCE().getDB().prepareStatement("select * from pokemon where lower(name) like ?");
			request.setString(1,'%'+n.toLowerCase()+'%');

			return findPokemons(request.executeQuery());
		}
	}

	/**
	 * @param result the resultset
	 * @param next if result have data inside of it
	 * @return Pokemon object from resultset
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 *
	 */
	private static Pokemon findPokemon(ResultSet result,boolean next) throws SQLException,IOException,ClassNotFoundException{
		if( ! next )
			return null;

		Pokemon p=new Pokemon();

		p.setId(result.getInt("id"));
		p.setName(result.getString("name"));

		// stats
		p.setHp(result.getInt("base_pv"));
		p.setAttack(result.getInt("base_atk"));
		p.setDefense(result.getInt("base_def"));
		p.setSpeAtk(result.getInt("base_spe_atk"));
		p.setSpeDef(result.getInt("base_spe_def"));
		p.setSpeed(result.getInt("base_spd"));

		// types
		p.setFirstType(result.getInt("first_type"));
		p.setSecondType(result.getInt("second_type"));

		// egg groups
		p.setFirstEggGroup(result.getInt("first_egg_group"));
		p.setSecondEggGroup(result.getInt("second_egg_group"));

		p.setSprite(result.getString("sprite"));
		p.setDescription(result.getString("description"));

		return p;
	}

	/**
	 * @param result the resultset
	 * @return Pokemon objects from resultset
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 *
	 */
	static List<Pokemon> findPokemons(ResultSet result) throws SQLException,IOException,ClassNotFoundException{
		List<Pokemon> pl=new ArrayList<>();
		Pokemon p=null;
		boolean haveNext=true;

		while( haveNext ){
			p=findPokemon(result,result.next());
			haveNext=(p!=null);

			if( haveNext )
				pl.add(p);
		}

		return pl;
	}
}
