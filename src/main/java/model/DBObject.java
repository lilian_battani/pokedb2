/**
 * 
 */
package model;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import core.PokeDBConnection;

/**
 * @author lilian battani
 * <p>Represent a database object that can either be a pokemon, an ability, etc ... </p>
 *
 */
public abstract class DBObject implements Comparable<DBObject>{

	/**
	 * id of newly created objects
	 *
	 */
	public final static int NEW_ID=-1;

	/**
	 * id for null foreign keys
	 *
	 */
	public final static int NO_ID=0;

	/**
	 * database connection
	 *
	 */
	private Connection db;

	/**
	 * object name
	 *
	 */
	private String name;

	/**
	 * object id
	 *
	 */
	private int id;

	/**
	 * create a new DB object set it's id to NEW_ID
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 *
	 */
	public DBObject() throws SQLException,IOException,ClassNotFoundException{
		this.db=PokeDBConnection.getINSTANCE().getDB();
		this.id=NEW_ID;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 *
	 *
	 */
	public int compareTo(DBObject dbo){
		if( dbo == null )
			return -1;

		if( dbo.getClass() != this.getClass() )
			return -1;

		return this.getId()-dbo.getId();
	}

	/**
	 * @return the db
	 */
	protected Connection getDb() {
		return db;
	}

	/**
	 * @param id the id to set
	 * @throws java.lang.IllegalArgumentException if id inferior or equal to 0
	 */
	protected void setId(int id) throws IllegalArgumentException{
		if( id <= 0 )
			throw new IllegalArgumentException("cannot insert id <= 0");

		this.id = id;
	}

	/**
	 * save this object into the database
	 * @throws java.sql.SQLException
	 *
	 */
	public abstract void save() throws SQLException;

	/**
	 * delete this object from the database
	 * @throws java.sql.SQLException
	 *
	 */
	public abstract void delete() throws SQLException;

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DBObject other = (DBObject) obj;
		if (id != other.id)
			return false;
		return true;
	}
}
