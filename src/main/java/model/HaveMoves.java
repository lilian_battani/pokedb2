/**
 * 
 */
package model;

import java.util.List;

import java.io.IOException;
import java.sql.SQLException;

/**
 * @author lilian battani
 * <p>Objects that have moves</p>
 */
public interface HaveMoves {

	/**
	 * @return object moves
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 *
	 */
	public List<Move> getMoves() throws IOException,SQLException,ClassNotFoundException;
}
