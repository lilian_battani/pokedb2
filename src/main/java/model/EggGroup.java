/**
 * 
 */
package model;

import java.util.List;
import java.util.ArrayList;

import java.io.IOException;

import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import core.PokeDBConnection;

import static model.DBObject.*;

/**
 * @author lilian battani
 * <p>Pokemon egg groups</p>
 */
public class EggGroup extends DBObject implements HavePokemons{

	/**
	 * create a new egg group
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 *
	 */
	private EggGroup() throws SQLException,IOException,ClassNotFoundException{
		super();
	}

	/**
	 * @see model.HavePokemons#getPokemons()
	 *
	 */
	public List<Pokemon> getPokemons() throws IOException,SQLException,ClassNotFoundException{
		synchronized(this.getDb()){
			PreparedStatement request=this.getDb().prepareStatement(
				"select * from pokemon\n"+
				"	where first_egg_group=?\n"+
				"	or second_egg_group=?;\n"
			);

			request.setInt(1,this.getId());
			request.setInt(2,this.getId());

			return Pokemon.findPokemons(request.executeQuery());
		}
	}

	/**
	 * @throws java.lang.IllegalStateException if the id is NEW_ID
	 * @see model.DBObject#save()
	 */
	@Override
	public void save() throws SQLException {
		// TODO Auto-generated method stub
		if( getId() == NEW_ID )
			throw new IllegalStateException(getId()+" not in egg_group");

		synchronized(this.getDb()){
			PreparedStatement request=this.getDb().prepareStatement(
				"update egg_group\n"+
				"	set name=?,\n"+
				"where id=?;\n"
			);

			request.setString(1,this.getName());
			request.setInt(2,this.getId());

			request.executeUpdate();
			// 
			this.getDb().commit();
		}
	}

	/* (non-Javadoc)
	 * @see model.DBObject#delete()
	 */
	@Override
	public void delete() throws SQLException {
		// TODO Auto-generated method stub
		synchronized(this.getDb()){
			PreparedStatement request=this.getDb().prepareStatement("delete from egg_group where id=?;");

			request.setInt(1,this.getId());
			request.executeUpdate();

			// 
			this.getDb().commit();
		}
	}

	/**
	 * find an egg group with its id
	 * @param id id of the ability to find
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 *
	 */
	public static EggGroup findById(int id) throws SQLException,IOException,ClassNotFoundException{
		synchronized(PokeDBConnection.getINSTANCE().getDB()){
			PreparedStatement request=PokeDBConnection.getINSTANCE().getDB().prepareStatement("select * from egg_group where id=?;");
			request.setInt(1,id);

			ResultSet rs=request.executeQuery();

			return findEggGroup(rs,rs.next());
		}
	}

	/**
	 * find an egg group with his name
	 * @param n name of the egg group to find
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 *
	 */
	public static EggGroup findByName(String n) throws SQLException,IOException,ClassNotFoundException{
		synchronized(PokeDBConnection.getINSTANCE().getDB()){
			PreparedStatement request=PokeDBConnection.getINSTANCE().getDB().prepareStatement("select * from egg_group where lower(name)=?;");
			request.setString(1,n.toLowerCase());

			ResultSet rs=request.executeQuery();

			return findEggGroup(rs,rs.next());
		}
	}

	/**
	 * find all egg groups
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 *
	 */
	public static List<EggGroup> findAll() throws SQLException,IOException,ClassNotFoundException{
		synchronized(PokeDBConnection.getINSTANCE().getDB()){
			Statement request=PokeDBConnection.getINSTANCE().getDB().createStatement();

			return findEggGroups(request.executeQuery("select * from egg_group;"));
		}
	}

	/**
	 * find egg groups with n String contained in his name
	 * @param n the String to find in ability name
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 *
	 */
	public static List<EggGroup> findByNameLike(String n) throws SQLException,IOException,ClassNotFoundException{
		synchronized(PokeDBConnection.getINSTANCE().getDB()){
			PreparedStatement request=PokeDBConnection.getINSTANCE().getDB().prepareStatement("select * from egg_group where lower(name) like ?");
			request.setString(1,'%'+n.toLowerCase()+'%');

			return findEggGroups(request.executeQuery());
		}
	}

	/**
	 * @param result the resultset
	 * @param next if result have data inside of it
	 * @return EggGroup object from resultset
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 *
	 */
	private static EggGroup findEggGroup(ResultSet result,boolean next) throws SQLException,IOException,ClassNotFoundException{
		if( ! next )
			return null;

		EggGroup e=new EggGroup();

		e.setId(result.getInt("id"));
		e.setName(result.getString("name"));

		return e;
	}

	/**
	 * @param result the resultset
	 * @return Ability objects from resultset
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 *
	 */
	static List<EggGroup> findEggGroups(ResultSet result) throws SQLException,IOException,ClassNotFoundException{
		List<EggGroup> al=new ArrayList<>();
		EggGroup e=null;
		boolean haveNext=true;

		while( haveNext ){
			e=findEggGroup(result,result.next());
			haveNext=(e!=null);

			if( haveNext )
				al.add(e);
		}

		return al;
	}
}
