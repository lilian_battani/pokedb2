/**
 * 
 */
package model;

import java.util.List;

import java.io.IOException;
import java.sql.SQLException;

/**
 * @author lilian battani
 * <p>Objects that have Pokemons</p>
 */
public interface HavePokemons {

	/**
	 * @return object pokemons
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 *
	 */
	public List<Pokemon> getPokemons() throws IOException,SQLException,ClassNotFoundException;

}
