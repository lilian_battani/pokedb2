/**
 * 
 */
package model;

import java.util.List;
import java.util.ArrayList;

import java.io.IOException;

import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import core.PokeDBConnection;

import static model.DBObject.*;

/**
 * @author lilian battani
 * <p>Pokemon Moves</p>
 */
public class Move extends DBObject implements HavePokemons{

	/**
	 * physical status
	 *
	 */
	public final static int PHYSICAL=0;

	/**
	 * special status
	 *
	 */
	public final static int SPECIAL=1;

	/**
	 * status status
	 *
	 */
	public final static int STATUS=2;

	/**
	 * move description
	 *
	 */
	private String description;

	/**
	 * move power
	 *
	 */
	private int power;

	/**
	 * move accuracy
	 *
	 */
	private int accuracy;

	/**
	 * move pp
	 *
	 */
	private int pp;

	/**
	 * move status
	 *
	 */
	private int status;

	/**
	 * move type id
	 *
	 */
	private int type;

	/**
	 * create a new move
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 *
	 */
	private Move() throws SQLException,IOException,ClassNotFoundException{
		super();
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @return the power
	 */
	public int getPower() {
		return power;
	}

	/**
	 * @return the accuracy
	 */
	public int getAccuracy() {
		return accuracy;
	}

	/**
	 * @return the pp
	 */
	public int getPp() {
		return pp;
	}

	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * @return the type
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 */
	public Type getType() throws IOException,SQLException,ClassNotFoundException{
		return Type.findById(type);
	}

	/**
	 * @see model.HavePokemons#getPokemons()
	 *
	 */
	public List<Pokemon> getPokemons() throws IOException,SQLException,ClassNotFoundException{
		synchronized(this.getDb()){
			PreparedStatement request=this.getDb().prepareStatement(
				"select * from pokemon,pokemon_move\n"+
				"	where pokemon_move.move=?\n"+
				"	and pokemon_move.pokemon=pokemon.id;\n"
			);

			request.setInt(1,this.getId());

			return Pokemon.findPokemons(request.executeQuery());
		}
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @param power the power to set
	 * @throws java.lang.IllegalArgumentException if power is lower than 0
	 */
	public void setPower(int power) throws IllegalArgumentException{
		if( power < 0 )
			throw new IllegalArgumentException(Integer.toString(power));

		this.power = power;
	}

	/**
	 * @param accuracy the accuracy to set
	 * @throws java.lang.IllegalArgumentException if accuracy isn't between 0 and 100
	 */
	public void setAccuracy(int accuracy) throws IllegalArgumentException{
		if( accuracy < 0 || accuracy > 100 )
			throw new IllegalArgumentException(Integer.toString(accuracy));

		this.accuracy = accuracy;
	}

	/**
	 * @param pp the pp to set
	 * @throws java.lang.IllegalArgumentException if pp is lower or equal to 0
	 */
	public void setPp(int pp) throws IllegalArgumentException{
		if( pp <= 0 )
			throw new IllegalArgumentException(Integer.toString(pp));

		this.pp = pp;
	}

	/**
	 * @param status the status to set
	 * @throws java.lang.IllegalArgumentException if status isn't PHYSICAL, SPECIAL or STATUS
	 */
	public void setStatus(int status) throws IllegalArgumentException{
		if( status != PHYSICAL && status != SPECIAL && status != STATUS )
			throw new IllegalArgumentException(Integer.toString(status));

		this.status = status;
	}

	/**
	 * @param type the type to set
	 * @throws java.lang.IllegalArgumentException if type is below or equal to 0
	 */
	public void setType(int type) throws IllegalArgumentException{
		if( type <= 0 )
			throw new IllegalArgumentException(Integer.toString(type));

		this.type = type;
	}

	/**
	 * @throws java.lang.IllegalStateException if the id is NEW_ID
	 * @see model.DBObject#save()
	 */
	@Override
	public void save() throws SQLException {
		// TODO Auto-generated method stub
		if( getId() == NEW_ID )
			throw new IllegalStateException(getId()+" not in move");

		synchronized(this.getDb()){
			PreparedStatement request=this.getDb().prepareStatement(
				"update move\n"+
				"	set name=?,\n"+
				"	description=?\n"+
				"	power=?\n"+
				"	accuracy=?\n"+
				"	pp=?\n"+
				"	status=?\n"+
				"	type=?\n"+
				"where id=?;\n"
			);

			request.setString(1,this.getName());
			request.setString(2,this.description);
			request.setInt(3,this.getPower());
			request.setInt(4,this.getAccuracy());
			request.setInt(5,this.getPp());
			request.setInt(6,this.getStatus());
			request.setInt(7,this.type);
			request.setInt(8,this.getId());

			request.executeUpdate();
			// 
			this.getDb().commit();
		}
	}

	/* (non-Javadoc)
	 * @see model.DBObject#delete()
	 */
	@Override
	public void delete() throws SQLException {
		// TODO Auto-generated method stub
		synchronized(this.getDb()){
			PreparedStatement request=this.getDb().prepareStatement("delete from move where id=?;");

			request.setInt(1,this.getId());
			request.executeUpdate();

			// 
			this.getDb().commit();
		}
	}

	/**
	 * find a move with its id
	 * @param id id of the move to find
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 *
	 */
	public static Move findById(int id) throws SQLException,IOException,ClassNotFoundException{
		synchronized(PokeDBConnection.getINSTANCE().getDB()){
			PreparedStatement request=PokeDBConnection.getINSTANCE().getDB().prepareStatement("select * from move where id=?;");
			request.setInt(1,id);

			ResultSet rs=request.executeQuery();

			return findMove(rs,rs.next());
		}
	}

	/**
	 * find a move with his name
	 * @param n name of the move to find
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 *
	 */
	public static Move findByName(String n) throws SQLException,IOException,ClassNotFoundException{
		synchronized(PokeDBConnection.getINSTANCE().getDB()){
			PreparedStatement request=PokeDBConnection.getINSTANCE().getDB().prepareStatement("select * from move where lower(name)=?;");
			request.setString(1,n.toLowerCase());

			ResultSet rs=request.executeQuery();

			return findMove(rs,rs.next());
		}
	}

	/**
	 * find all moves
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 *
	 */
	public static List<Move> findAll() throws SQLException,IOException,ClassNotFoundException{
		synchronized(PokeDBConnection.getINSTANCE().getDB()){
			Statement request=PokeDBConnection.getINSTANCE().getDB().createStatement();

			return findMoves(request.executeQuery("select * from move;"));
		}
	}

	/**
	 * find moves with n String contained in his name
	 * @param n the String to find in move name
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 *
	 */
	public static List<Move> findByNameLike(String n) throws SQLException,IOException,ClassNotFoundException{
		synchronized(PokeDBConnection.getINSTANCE().getDB()){
			PreparedStatement request=PokeDBConnection.getINSTANCE().getDB().prepareStatement("select * from move where lower(name) like ?");
			request.setString(1,'%'+n.toLowerCase()+'%');

			return findMoves(request.executeQuery());
		}
	}

	/**
	 * @param status the move status
	 * @return move status String or null if status isn't a valid status (PHYSICAL,SPECIAL or STATUS)
	 *
	 */
	public static String getStatusString(int status){
		if( status == PHYSICAL )
			return "Physique";

		if( status == SPECIAL )
			return "Spécial";

		if( status == STATUS )
			return "statut";

		return null;
	}

	/**
	 * @param result the resultset
	 * @param next if result have data inside of it
	 * @return Move object from resultset
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 *
	 */
	static Move findMove(ResultSet result,boolean next) throws SQLException,IOException,ClassNotFoundException{
		if( ! next )
			return null;

		Move m=new Move();

		m.setId(result.getInt("id"));
		m.setName(result.getString("name"));
		m.setDescription(result.getString("description"));
		m.setPower(result.getInt("power"));
		m.setAccuracy(result.getInt("accuracy"));
		m.setPp(result.getInt("pp"));
		m.setStatus(result.getInt("status"));
		m.setType(result.getInt("type"));

		return m;
	}

	/**
	 * @param result the resultset
	 * @return Move objects from resultset
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 *
	 */
	static List<Move> findMoves(ResultSet result) throws SQLException,IOException,ClassNotFoundException{
		List<Move> ml=new ArrayList<>();
		Move m=null;
		boolean haveNext=true;

		while( haveNext ){
			m=findMove(result,result.next());
			haveNext=(m!=null);

			if( haveNext )
				ml.add(m);
		}

		return ml;
	}

}
