/**
 * 
 */
package model;

import java.util.List;
import java.util.ArrayList;

import java.io.IOException;

import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import core.PokeDBConnection;

import static model.DBObject.*;

/**
 * @author lilian battani
 * <p>Pokemon Ability</p>
 */
public class Ability extends DBObject implements HavePokemons{

	/**
	 * ability description
	 *
	 */
	private String description;

	/**
	 * create a new ability
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 *
	 */
	private Ability() throws SQLException,IOException,ClassNotFoundException{
		super();
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @see model.HavePokemons#getPokemons()
	 *
	 */
	public List<Pokemon> getPokemons() throws IOException,SQLException,ClassNotFoundException{
		synchronized(this.getDb()){
			PreparedStatement request=this.getDb().prepareStatement(
				"select * from pokemon,have_ability\n"+
				"	where pokemon.id=have_ability.pokemon\n"+
				"	and have_ability.ability=?;\n"
			);

			request.setInt(1,this.getId());

			return Pokemon.findPokemons(request.executeQuery());
		}
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @throws java.lang.IllegalStateException if the id is NEW_ID
	 * @see model.DBObject#save()
	 */
	@Override
	public void save() throws SQLException {
		// TODO Auto-generated method stub
		if( getId() == NEW_ID )
			throw new IllegalStateException(getId()+" not in ability");

		synchronized(this.getDb()){

			PreparedStatement request=this.getDb().prepareStatement(
				"update ability\n"+
				"	set name=?,\n"+
				"	description=?\n"+
				"where id=?;\n"
			);

			request.setString(1,this.getName());
			request.setString(2,this.description);
			request.setInt(3,this.getId());

			request.executeUpdate();
			// 
			this.getDb().commit();
		}
	}

	/* (non-Javadoc)
	 * @see model.DBObject#delete()
	 */
	@Override
	public void delete() throws SQLException {
		// TODO Auto-generated method stub
		synchronized(this.getDb()){
			PreparedStatement request=this.getDb().prepareStatement("delete from ability where id=?;");

			request.setInt(1,this.getId());
			request.executeUpdate();

			// 
			this.getDb().commit();
		}
	}

	/**
	 * find an ability with its id
	 * @param id id of the ability to find
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 *
	 */
	public static Ability findById(int id) throws SQLException,IOException,ClassNotFoundException{
		synchronized(PokeDBConnection.getINSTANCE().getDB()){
			PreparedStatement request=PokeDBConnection.getINSTANCE().getDB().prepareStatement("select * from ability where id=?;");
			request.setInt(1,id);

			ResultSet rs=request.executeQuery();

			return findAbility(rs,rs.next());
		}
	}

	/**
	 * find an ability with his name
	 * @param n name of the ability to find
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 *
	 */
	public static Ability findByName(String n) throws SQLException,IOException,ClassNotFoundException{
		synchronized(PokeDBConnection.getINSTANCE().getDB()){
			PreparedStatement request=PokeDBConnection.getINSTANCE().getDB().prepareStatement("select * from ability where lower(name)=?;");
			request.setString(1,n.toLowerCase());

			ResultSet rs=request.executeQuery();

			return findAbility(rs,rs.next());
		}
	}

	/**
	 * find all abilities
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 *
	 */
	public static List<Ability> findAll() throws SQLException,IOException,ClassNotFoundException{
		synchronized(PokeDBConnection.getINSTANCE().getDB()){
			Statement request=PokeDBConnection.getINSTANCE().getDB().createStatement();

			return findAbilities(request.executeQuery("select * from ability;"));
		}
	}

	/**
	 * find abilities with n String contained in his name
	 * @param n the String to find in ability name
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 *
	 */
	public static List<Ability> findByNameLike(String n) throws SQLException,IOException,ClassNotFoundException{
		synchronized(PokeDBConnection.getINSTANCE().getDB()){
			PreparedStatement request=PokeDBConnection.getINSTANCE().getDB().prepareStatement("select * from ability where lower(name) like ?");
			request.setString(1,'%'+n.toLowerCase()+'%');

			return findAbilities(request.executeQuery());
		}
	}

	/**
	 * @param result the resultset
	 * @param next if result have data inside of it
	 * @return Ability object from resultset
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 *
	 */
	static Ability findAbility(ResultSet result,boolean next) throws SQLException,IOException,ClassNotFoundException{
		if( ! next )
			return null;

		Ability a=new Ability();

		a.setId(result.getInt("id"));
		a.setName(result.getString("name"));
		a.setDescription(result.getString("description"));

		return a;
	}

	/**
	 * @param result the resultset
	 * @return Ability objects from resultset
	 * @throws java.io.IOException
	 * @throws java.sql.SQLException
	 * @throws java.lang.ClassNotFoundException if mysql driver is not found
	 *
	 */
	static List<Ability> findAbilities(ResultSet result) throws SQLException,IOException,ClassNotFoundException{
		List<Ability> al=new ArrayList<>();
		Ability a=null;
		boolean haveNext=true;

		while( haveNext ){
			a=findAbility(result,result.next());
			haveNext=(a!=null);

			if( haveNext )
				al.add(a);
		}

		return al;
	}

}
