/**
 *
 */
package jnet.apiclient;

import java.util.Scanner;

import java.io.IOException;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;

import com.google.gson.JsonParser;
import com.google.gson.JsonElement;

/**
 * @author lilian battani
 * <p>API client</p>
 */
public class APIClient {

    /**
     * URL of the api
     *
     */
    private APIUrl url;

    /**
     * connection to the api
     *
     */
    private HttpURLConnection connection;

    /**
     * parser that generate the json elements
     *
     */
    private JsonParser parser;

    /**
     * current json element
     *
     */
    private JsonElement current;

    /**
     * true if the connection has been updated
     *
     */
    private boolean updated;

    /**
     * @param apiUrl url of the api
     * @throws java.lang.NullPointerException if apiUrl is null
     * @throws java.net.MalformedURLException
     *
     */
    public APIClient(String apiUrl) throws NullPointerException,MalformedURLException{
        this.parser=new JsonParser();
        this.url=new APIUrl(apiUrl);
    }

    /**
     * @return the url of this api
     *
     */
    public APIUrl getAPIUrl(){
        return url;
    }

    /**
     * @return the json element corresponding to the current url
     * @throws java.lang.IllegalStateException if the connection has not
     * been enabled with updateConnection
     * @throws java.io.IOException
     *
     */
    public synchronized JsonElement get() throws IllegalStateException,IOException{
        if( connection == null )
            throw new IllegalStateException("connection not enabled");

        if( updated ){
            String response="";
            Scanner s=new Scanner(connection.getInputStream());

            while( s.hasNextLine() )
                response+=s.nextLine();

            current=parser.parse(response);
            updated=false;
        }

        return current;
    }

    /**
     * update the connection to the api with its url
     * @throws java.io.IOException
     *
     */
    public synchronized void updateConnection() throws IOException{
        connection=url.generateAPIConnection();
        updated=true;
    }
}
