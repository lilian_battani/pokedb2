/**
 *
 */
package jnet.apiclient;

import java.util.LinkedList;

import java.io.IOException;

import java.net.URL;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;

/**
 * @author lilian battani
 * <p>Api url class</p>
 */
public class APIUrl {

    /**
     * protocol (http or https)
     *
     */
    private String protocol;

    /**
     * host of the api
     *
     */
    private String host;

    /**
     * port of the api
     *
     */
    private int port;

    /**
     * url path
     *
     */
    private LinkedList<String> path;

    /**
     * if there is no file in the path
     *
     */
    private boolean noFile;

    /**
     * @param url url String for this api
     * @throws java.lang.NullPointerException if url is null
     * @throws java.net.MalformedURLException
     *
     */
    public APIUrl(String url) throws NullPointerException,MalformedURLException{
        if( url == null )
            throw new NullPointerException("null");

        URL u=new URL(url);
        noFile=true;
        path=new LinkedList<>();

        protocol=u.getProtocol();
        host=u.getHost();
        port=u.getDefaultPort();

        if( u.getPath() != null )
            for( String p : u.getPath().split("/") )
                if( p != null && p.length() > 0 )
                    path.addLast(p);
    }

    /**
     * @return the protocol
     */
    public String getProtocol() {
        return protocol;
    }

    /**
     * @return the host
     */
    public String getHost() {
        return host;
    }

    /**
     * @return the port
     */
    public int getPort() {
        return port;
    }

    /**
     * @return the path
     */
    public LinkedList<String> getPath() {
        return path;
    }

    /**
     * @return true if this url have a file in this path
     *
     */
    public boolean hasFile(){
        return ! noFile;
    }

    /**
     * @return the path length (number of elements in the path)
     *
     */
    public int pathLength(){
        return path.size();
    }

    /**
     * convert to URL
     * @return URL conversion
     * @throws java.net.MalformedURLException
     */
    public URL toURL() throws MalformedURLException{
        String url=protocol+"://";
        url+=host;
        // url+=":"+port;

        for( String p : path )
            url+="/"+p;

        if( noFile )
            url+="/";

        return new URL(url);
    }

    /**
     * @return http connection to the api
     * @throws java.io.IOException
     */
    public HttpURLConnection generateAPIConnection() throws IOException{
        try{
            URL u=toURL();
            HttpURLConnection http=(HttpURLConnection)u.openConnection();
            http.addRequestProperty("User-Agent","Mozilla/5.0");
            http.setRequestMethod("GET");
            http.setDoOutput(true);
            http.connect();

            return http;
        }catch(MalformedURLException mue){
            throw new RuntimeException(mue);
        }
    }

    /**
     * add an element to path
     * @param pe path element to add
     * @throws java.lang.NullPointerException if pe is null
     * @throws java.lang.IllegalArgumentException if pe is empty
     *
     */
    public synchronized APIUrl addToPath(String pe) throws NullPointerException{
        if( pe == null )
            throw new NullPointerException("null");

        if( pe.equals("") )
            throw new IllegalArgumentException("empty string");

        path.addLast(pe);

        try{
            toURL();
        }catch(MalformedURLException mue){
            delFromPath();
            throw new IllegalArgumentException(pe);
        }

        return this;
    }

    /**
     * delete a path element from this url
     * @param pe path element to delete
     *
     */
    public synchronized APIUrl delFromPath(String pe){
        path.remove(pe);
        return this;
    }

    /**
     * delete a path element from this url
     * @param i index of the path element to delete
     *
     */
    public synchronized APIUrl delFromPath(int i){
        path.remove(i);
        return this;
    }

    /**
     * delete the last path element from this url
     *
     */
    public synchronized APIUrl delFromPath(){
        path.removeLast();
        return this;
    }

    /**
     * set noFile value to false
     * @param has true if hasFile
     */
    public synchronized void setHasFile(boolean has){
        noFile=!has;
    }
}

