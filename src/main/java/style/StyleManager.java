package style;

import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;

/**
 *
 */
public class StyleManager {

    /**
     *
     * @param hb
     */
    public static HBox hbox(HBox hb){
        hb.setBackground(new Background(new BackgroundFill(Color.web("#95d9de"),new CornerRadii(5),null)));
        hb.setStyle("-fx-font-family: Algerian;"+
                "-fx-padding: 10;" +
                "-fx-border-style: solid inside;" +
                "-fx-border-width: 2;" +
                "-fx-border-insets: 5;" +
                "-fx-border-radius: 5;" +
                "-fx-border-color: blue;");

        return hb;
    }
}
