package gui.controller;

import gui.StageManager;
import gui.view.AbilitySimpleCell;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import model.Ability;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import static core.ListenerUtils.*;

public class AbilitySearchController extends SearchController {

    /**
     *
     * @throws SQLException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    @Override
    public void search() throws SQLException, IOException, ClassNotFoundException {
        List<Ability> la = Ability.findByNameLike(this.content.getText());

        this.results.getItems().clear();
        this.results.getItems().addAll(la);
    }

    /**
     *
     * @throws SQLException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    @Override
    public void initialize() throws SQLException, IOException, ClassNotFoundException {
        this.results.setCellFactory(AbilitySimpleCell.factory());

        this.results.getItems().addAll(Ability.findAll());
        addAbilityListListener(results);
    }
}
