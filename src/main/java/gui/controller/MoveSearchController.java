package gui.controller;

import gui.StageManager;
import gui.view.MoveSimpleCell;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import model.DBObject;
import model.Move;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import static core.ListenerUtils.*;

/**
 *
 */
public class MoveSearchController extends SearchController {

    /**
     *
     * @throws SQLException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    @Override
    public void search() throws SQLException, IOException, ClassNotFoundException {
        List<Move> lm = Move.findByNameLike(this.content.getText());

        this.results.getItems().clear();
        this.results.getItems().addAll(lm);
    }

    /**
     *
     * @throws SQLException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    @Override
    public void initialize() throws SQLException, IOException, ClassNotFoundException {
        this.results.setCellFactory(MoveSimpleCell.factory());

        this.results.getItems().addAll(Move.findAll());
        addMoveListListener(results);
    }
}
