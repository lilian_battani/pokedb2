package gui.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import model.DBObject;
import model.NotePad;

import javafx.event.ActionEvent;
import java.io.IOException;
import java.sql.SQLException;

/**
 *
 */
public class NotePadController extends Controller {

    /**
     *
     */
    @FXML
    private TextField title;

    /**
     *
     */
    @FXML
    private TextArea content;

    /**
     *
     */
    @FXML
    private Button edit;

    /**
     *
     */
    @FXML
    private Button display;

    /**
     *
     */
    @FXML
    private Button save;

    /**
     *
     */
    private NotePad notePad;

    /**
     *
     * @param np
     */
    public NotePadController(NotePad np){
        if( np != null )
            this.notePad=np;
        else
            try {
                this.notePad = new NotePad();
            }catch(SQLException sqle){

            }catch(IOException ioe){

            }catch (ClassNotFoundException cnfe){

            }
    }

    /**
     *
     */
    public NotePadController(){
        this(null);
    }

    /**
     *
     * @throws SQLException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    @Override
    public void initialize() throws SQLException, IOException, ClassNotFoundException {
        title.setText(notePad.getName());
        content.setText(notePad.getContent());

        //
        if( notePad.getId() == DBObject.NEW_ID ){
            this.editPad(null);
        }else{
            this.displayPad(null);
        }
    }

    /**
     *
     * @param event
     */
    public void editPad(ActionEvent event){
        edit.setDisable(true);
        display.setDisable(false);
        save.setDisable(false);

        title.setEditable(true);
        content.setEditable(true);
    }

    /**
     *
     * @param event
     */
    public void displayPad(ActionEvent event){
        edit.setDisable(false);
        display.setDisable(true);
        save.setDisable(true);

        title.setEditable(false);
        content.setEditable(false);
    }

    /**
     *
     * @param event
     */
    public void savePad(ActionEvent event){
        if( title.getText().length() > 0 ) {
            notePad.setName(title.getText());
            notePad.setContent(content.getText());

            try {
                notePad.save();
            } catch (SQLException sqle) {
            }
        }
    }
}
