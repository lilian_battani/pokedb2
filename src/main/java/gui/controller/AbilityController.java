package gui.controller;

import gui.StageManager;
import gui.view.AbilitySimpleCell;
import gui.view.PokemonSimpleCell;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import model.Ability;
import model.DBObject;
import model.Pokemon;

import java.io.IOException;
import java.sql.SQLException;

import static core.ListenerUtils.*;

/**
 * controller for abilities display
 */
public class AbilityController extends Controller{

    /**
     *
     */
    @FXML
    private Label name;

    /**
     *
     */
    @FXML
    private Label descr;

    /**
     *
     */
    @FXML
    private ListView<DBObject> pokemons;

    /**
     * displayed ability
     */
    private Ability ability;

    /**
     *
     * @param ability
     */
    public AbilityController(Ability ability){
        this.ability=ability;
    }

    /**
     *
     * @throws SQLException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    @Override
    public void initialize() throws SQLException, IOException, ClassNotFoundException {

        pokemons.setCellFactory(PokemonSimpleCell.factory());
        //
        name.setText(ability.getName());
        descr.setText(ability.getDescription());

        pokemons.getItems().addAll(ability.getPokemons());
        addPokemonListListener(pokemons);
    }
}
