package gui.controller;

import gui.StageManager;
import gui.view.PokemonSimpleCell;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import model.Pokemon;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import static core.ListenerUtils.*;

/**
 *
 */
public class PokemonSearchController extends SearchController {



    /**
     *
     */
    @Override
    public void search() throws SQLException, IOException,ClassNotFoundException {
        List<Pokemon> lp = Pokemon.findByNameLike(this.content.getText());

        //
        this.results.getItems().clear();
        this.results.getItems().addAll(lp);
    }

    /**
     *
     */
    @Override
    public void initialize() throws SQLException, IOException,ClassNotFoundException {
        this.results.setCellFactory(PokemonSimpleCell.factory());

        this.results.getItems().addAll(Pokemon.findAll());
        addPokemonListListener(results);
    }
}
