package gui.controller;

import gui.view.NotePadSimpleCell;
import model.NotePad;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import static core.ListenerUtils.*;

public class NotePadSearchController extends SearchController {

    /**
     *
     * @throws SQLException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    @Override
    public void search() throws SQLException, IOException, ClassNotFoundException {
        List<NotePad> ln = NotePad.findByNameLike(this.content.getText());

        this.results.getItems().clear();
        this.results.getItems().addAll(ln);
    }

    /**
     *
     * @throws SQLException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    @Override
    public void initialize() throws SQLException, IOException, ClassNotFoundException {
        this.results.setCellFactory(NotePadSimpleCell.factory());

        this.results.getItems().addAll(NotePad.findAll());
        addNotePadListListener(results);
    }
}
