package gui.controller;

import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import model.DBObject;
import model.Pokemon;

import java.io.IOException;
import java.sql.SQLException;

/**
 * search controller
 */
public abstract class SearchController extends Controller {

    /**
     *
     */
    @FXML
    protected ListView<DBObject> results;

    /**
     *
     */
    @FXML
    protected TextField content;

    /**
     * search method
     */
    public abstract void search() throws SQLException, IOException,ClassNotFoundException;
}
