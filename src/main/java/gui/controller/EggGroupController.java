package gui.controller;

import gui.StageManager;
import gui.view.PokemonSimpleCell;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import model.DBObject;
import model.EggGroup;
import model.Pokemon;

import java.io.IOException;
import java.sql.SQLException;

import static core.ListenerUtils.*;

/**
 * controller for egg group display
 */
public class EggGroupController extends Controller {

    /**
     *
     */
    @FXML
    private Label name;

    /**
     *
     */
    @FXML
    private ListView<DBObject> pokemons;

    /**
     *
     */
    private EggGroup eg;

    /**
     *
     * @param eg
     */
    public EggGroupController(EggGroup eg){
        this.eg=eg;
    }

    /**
     *
     * @throws SQLException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    @Override
    public void initialize() throws SQLException, IOException, ClassNotFoundException {
        this.pokemons.setCellFactory(PokemonSimpleCell.factory());
        //
        this.name.setText(this.eg.getName());
        this.pokemons.getItems().addAll(this.eg.getPokemons());
        addPokemonListListener(pokemons);
    }
}
