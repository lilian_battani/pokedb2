package gui.controller;

import javafx.fxml.Initializable;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

/**
 *
 */
public abstract class Controller implements Initializable {

    @Override // and this
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        try {
            this.initialize();
        }catch(SQLException sqle){

        }catch (IOException ioe){

        }catch (ClassNotFoundException cnfe){

        }
    }

    /**
     *
     */
    public abstract void initialize() throws SQLException, IOException,ClassNotFoundException;
}
