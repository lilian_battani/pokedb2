package gui.controller;

import core.Scaler;
import gui.StageManager;
import gui.view.MoveSimpleCell;
import gui.view.PokemonSimpleCell;
import gui.view.TypeSimpleCell;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import model.DBObject;
import model.Move;
import model.Pokemon;
import model.Type;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

import static core.ListenerUtils.*;

/**
 * controller for pokemon type display
 */
public class TypeController extends Controller{

    /**
     *
     */
    @FXML
    private ImageView type1;

    /**
     *
     */
    @FXML
    private ImageView type2;

    /**
     *
     */
    @FXML
    private ListView<DBObject> immunities;

    /**
     *
     */
    @FXML
    private ListView<DBObject> doubleResistances;

    /**
     *
     */
    @FXML
    private ListView<DBObject> resistances;

    /**
     *
     */
    @FXML
    private ListView<DBObject> weaknesses;

    /**
     *
     */
    @FXML
    private ListView<DBObject> doubleWeaknesses;

    /**
     *
     */
    @FXML
    private ListView<DBObject> pokemons;

    /**
     *
     */
    @FXML
    private ListView<DBObject> moves;

    /**
     *
     */
    @FXML
    private CheckBox atkDef;

    /**
     *
     */
    private Type t1;

    /**
     *
     */
    private Type t2;

    /**
     *
     * @param type1
     * @param type2
     */
    public TypeController(Type type1,Type type2){
        this.t1=type1;
        this.t2=type2;
    }

    /**
     *
     * @throws SQLException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    @Override
    public void initialize() throws SQLException, IOException, ClassNotFoundException {
        immunities.setCellFactory(TypeSimpleCell.factory());
        doubleResistances.setCellFactory(TypeSimpleCell.factory());
        resistances.setCellFactory(TypeSimpleCell.factory());
        weaknesses.setCellFactory(TypeSimpleCell.factory());
        doubleWeaknesses.setCellFactory(TypeSimpleCell.factory());

        Scaler s = new Scaler();

        pokemons.setCellFactory(PokemonSimpleCell.factory());

        moves.setCellFactory(MoveSimpleCell.factory());

        this.defender();

        // types images

        Image imt1 = new Image(new FileInputStream(t1.getSprite()));
        Image imt2 = null;

        s.add(type1,imt1,0.5);

        if( t2 != null ) {
            imt2 = new Image(new FileInputStream(t2.getSprite()));

            s.add(type2,imt2,0.5);
        }

        // type affinities

        //

        // pokemons

        List<Pokemon> poks = t1.getPokemons();

        if( t2 != null ){
            final List<Pokemon> poks2 = t2.getPokemons();

            poks = poks.stream().filter( p -> poks2.contains(p)).collect(Collectors.toList());
        }

        pokemons.getItems().addAll(poks);

        //

        // moves

        List<Move> movs = t1.getMoves();

        if( t2 != null )
            movs.addAll(t2.getMoves());

        moves.getItems().addAll(movs);

        addTypeListListener(immunities);
        addTypeListListener(doubleResistances);
        addTypeListListener(resistances);
        addTypeListListener(weaknesses);
        addTypeListListener(doubleWeaknesses);
        addMoveListListener(moves);
        addPokemonListListener(pokemons);
    }

    /**
     *
     * @param event
     */
    public void switchAtkDef(ActionEvent event) throws SQLException, IOException, ClassNotFoundException{
        this.clearTypes();

        if( atkDef.isSelected() )
            this.attacker();
        else
            this.defender();
    }

    /**
     *
     * @throws SQLException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void defender() throws SQLException, IOException, ClassNotFoundException{
        Map<Type,Double> affinities = t1.getAffinitiesFrom();

        this.ifSecondType((e1,e2)->e1*e2,affinities,t -> {
            try{
                return t.getAffinitiesFrom();
            }catch (IOException ioe){
            }catch (SQLException sqle){
            }catch (ClassNotFoundException cnfe){
            }

            return null;
        });
        this.fillAffinities(affinities);
    }

    /**
     *
     * @throws SQLException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void attacker() throws SQLException, IOException, ClassNotFoundException{
        Map<Type,Double> affinities = t1.getAffinitiesTo();

        this.ifSecondType((e1,e2)->Math.max(e1,e2),affinities,t -> {
            try{
                return t.getAffinitiesTo();
            }catch (IOException ioe){
            }catch (SQLException sqle){
            }catch (ClassNotFoundException cnfe){
            }

            return null;
        });
        this.fillAffinities(affinities);
    }

    /**
     *
     */
    private void clearTypes(){
        immunities.getItems().clear();
        doubleResistances.getItems().clear();
        resistances.getItems().clear();
        doubleWeaknesses.getItems().clear();
        weaknesses.getItems().clear();
    }

    /**
     *
     * @param affinities
     */
    private void fillAffinities(Map<Type,Double> affinities){
        for( Map.Entry<Type,Double> e : affinities.entrySet() )
            if( e.getValue() == Type.IMMUNE )
                immunities.getItems().add(e.getKey());
            else if( e.getValue() == Type.DOUBLE_RESISTS )
                doubleResistances.getItems().add(e.getKey());
            else if( e.getValue() == Type.RESISTS )
                resistances.getItems().add(e.getKey());
            else if( e.getValue() == Type.SUPER_EFFICIENT )
                weaknesses.getItems().add(e.getKey());
            else if( e.getValue() == Type.DOUBLE_SUPER_EFFICIENT )
                doubleWeaknesses.getItems().add(e.getKey());
    }

    /**
     *
     * @param newAff
     */
    private void ifSecondType(BiFunction<Double,Double,Double> newAff, Map<Type,Double> affinities, Function<Type,Map<Type, Double>> getaff2) throws SQLException,IOException,ClassNotFoundException{
        if( t2 != null ){
            final Map<Type,Double> aff2 = getaff2.apply(t2);

            List<Map.Entry<Type,Double>> entr = new ArrayList<>(affinities.entrySet());

            // build double type affinities
            for( Map.Entry<Type,Double> entry : entr )
                affinities.put(entry.getKey(), newAff.apply(entry.getValue(), aff2.get(entry.getKey())));
        }
    }
}
