package gui.controller;

import gui.StageManager;
import gui.view.TypeSimpleCell;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import model.Type;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import static core.ListenerUtils.*;

public class TypeSearchController extends SearchController {

    /**
     *
     * @throws SQLException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    @Override
    public void search() throws SQLException, IOException, ClassNotFoundException {
        List<Type> lt = Type.findByNameLike(this.content.getText());

        this.results.getItems().clear();
        this.results.getItems().addAll(lt);
    }

    /**
     *
     * @throws SQLException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    @Override
    public void initialize() throws SQLException, IOException, ClassNotFoundException {
        this.results.setCellFactory(TypeSimpleCell.factory());

        this.results.getItems().addAll(Type.findAll());
        addTypeListListener(results);
    }
}
