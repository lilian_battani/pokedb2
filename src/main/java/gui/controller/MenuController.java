package gui.controller;

import core.PokeDBConnection;
import gui.StageManager;
import javafx.application.Platform;

import java.io.IOException;
import java.sql.SQLException;

/**
 *
 */
public class MenuController {

    /**
     *
     */
    public MenuController(){
    }

    /**
     *
     */
    public void help(){

    }

    /**
     *
     */
    public void about(){

    }

    /**
     *
     */
    public void home(){
        StageManager.get().home();
    }

    /**
     *
     */
    public void quit(){
        Platform.exit();
    }

    /**
     *
     */
    public void searchPokemon(){
        StageManager.get().searchPokemon();
    }

    /**
     *
     */
    public void searchAbility(){
        StageManager.get().searchAbility();
    }

    /**
     *
     */
    public void searchEggGroup(){
        StageManager.get().searchEggGroup();
    }

    /**
     *
     */
    public void searchMove(){
        StageManager.get().searchMove();
    }

    /**
     *
     */
    public void searchType(){
        StageManager.get().searchType();
    }

    /**
     *
     */
    public void searchNotePad(){
        StageManager.get().searchNotePad();
    }

    /**
     *
     */
    public void params(){
        StageManager.get().params();
    }
}
