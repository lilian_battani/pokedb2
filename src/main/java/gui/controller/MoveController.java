package gui.controller;

import core.Scaler;
import gui.StageManager;
import gui.view.PokemonSimpleCell;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import model.DBObject;
import model.Move;
import model.Pokemon;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;

import static core.ListenerUtils.*;

/**
 * controller for move display
 */
public class MoveController extends Controller {

    /**
     *
     */
    @FXML
    private Label name;

    /**
     *
     */
    @FXML
    private Label descr;

    /**
     *
     */
    @FXML
    private ImageView type;

    /**
     *
     */
    @FXML
    private Label power;

    /**
     *
     */
    @FXML
    private Label accuracy;

    /**
     *
     */
    @FXML
    private Label pp;

    /**
     *
     */
    @FXML
    private Label status;

    /**
     *
     */
    @FXML
    private ListView<DBObject> pokemons;

    /**
     *
     */
    private Move move;

    /**
     *
     * @param move
     */
    public MoveController(Move move){
        this.move=move;
    }

    /**
     *
     * @throws SQLException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    @Override
    public void initialize() throws SQLException, IOException, ClassNotFoundException {
        Scaler s = new Scaler();

        pokemons.setCellFactory(PokemonSimpleCell.factory());
        //
        name.setText(move.getName());
        descr.setText(move.getDescription());

        // type image
        Image im = new Image(new FileInputStream(move.getType().getSprite()));

        s.add(type,im,0.5);

        //
        power.setText(Integer.toString(move.getPower()));
        accuracy.setText(Integer.toString(move.getAccuracy()));
        pp.setText(Integer.toString(move.getPp()));
        status.setText(Move.getStatusString(move.getStatus()));

        pokemons.getItems().addAll(move.getPokemons());


        addTypeImageListener(type,move.getType(),null);
        addPokemonListListener(pokemons);
    }
}
