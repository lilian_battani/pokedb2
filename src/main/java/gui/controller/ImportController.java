package gui.controller;

import core.PokeDBConnection;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;

import java.io.IOException;
import java.sql.SQLException;

/**
 * controller for params window
 */
public class ImportController extends Controller {

    /**
     *
     */
    @FXML
    private ProgressBar loading;

    /**
     *
     */
    @FXML
    private Label loaded;

    /**
     *
     */
    public ImportController(){

    }

    /**
     *
     * @throws SQLException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    @Override
    public void initialize() throws SQLException, IOException, ClassNotFoundException {
        loading.setDisable(true);
        loaded.setDisable(true);

        this.addLoadingListener();
        this.addLoadedListener();
    }

    /**
     *
     * @param event
     */
    public void createDB(ActionEvent event) throws SQLException,IOException,ClassNotFoundException{
       PokeDBConnection.getINSTANCE().create();
    }

    /**
     *
     * @param event
     */
    public void importData(ActionEvent event) throws SQLException,IOException,ClassNotFoundException{
        PokeDBConnection.getINSTANCE().refresh();
    }

    /**
     *
     */
    private void addLoadingListener(){
        try {
            PokeDBConnection.getINSTANCE().loadingProperty().addListener(new ChangeListener<Boolean>() {
                @Override
                public void changed(ObservableValue<? extends Boolean> observableValue, Boolean aBoolean, Boolean t1) {

                    if( t1 ){
                        loading.setDisable(false);
                        loaded.setDisable(false);
                    }else{
                        loading.setProgress(0);
                        loaded.setText("");

                        loading.setDisable(true);
                        loaded.setDisable(true);
                    }
                }
            });
        }catch(SQLException sqle){
        }catch(IOException ioe){
        }catch(ClassNotFoundException cnfe){}
    }

    /**
     *
     */
    private void addLoadedListener(){
        try{
            final int count = PokeDBConnection.getINSTANCE().importerCount();

            PokeDBConnection.getINSTANCE().loadedProperty().addListener(new ChangeListener<Number>() {
                @Override
                public void changed(ObservableValue<? extends Number> observableValue, Number number, Number t1) {
                    System.out.println("loaded");

                    // usefull values
                    double total = (double)count;
                    double current = (double)t1.intValue();

                    //
                    loading.setProgress(current/total);
                    loaded.setText("chargement "+(int)current+"/"+(int)total);
                }
            });
        }catch(SQLException sqle){
        }catch(IOException ioe){
        }catch(ClassNotFoundException cnfe){}
    }
}
