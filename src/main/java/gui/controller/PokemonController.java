package gui.controller;

import core.Scaler;
import gui.StageManager;
import gui.view.MoveSimpleCell;
import gui.view.PokemonSimpleCell;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import model.*;

import java.awt.print.PrinterGraphics;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static core.ListenerUtils.*;
/**
 *
 */
public class PokemonController extends Controller {

    /**
     *
     */
    @FXML
    private ImageView pokeSprite;

    /**
     *
     */
    @FXML
    private Label pokeName;

    /**
     *
     */
    @FXML
    private ImageView pokeType1;

    /**
     *
     */
    @FXML
    private ImageView pokeType2;

    /**
     *
     */
    @FXML
    private Label ability1;

    /**
     *
     */
    @FXML
    private Label ability2;

    /**
     *
     */
    @FXML
    private Label abilityH;

    /**
     *
     */
    @FXML
    private Label eggGroup1;

    /**
     *
     */
    @FXML
    private Label eggGroup2;

    /**
     *
     */
    @FXML
    private ListView<DBObject> evolutions;

    /**
     *
     */
    @FXML
    private ProgressBar pv;

    /**
     *
     */
    @FXML
    private Label pv_label;

    /**
     *
     */
    @FXML
    private ProgressBar atk;

    /**
     *
     */
    @FXML
    private Label atk_label;

    /**
     *
     */
    @FXML
    private ProgressBar def;

    /**
     *
     */
    @FXML
    private Label def_label;

    /**
     *
     */
    @FXML
    private ProgressBar spa;

    /**
     *
     */
    @FXML
    private Label spa_label;

    /**
     *
     */
    @FXML
    private ProgressBar spd;

    /**
     *
     */
    @FXML
    private Label spd_label;

    /**
     *
     */
    @FXML
    private ProgressBar vit;

    /**
     *
     */
    @FXML
    private Label vit_label;

    /**
     *
     */
    @FXML
    private Label basestat;

    /**
     *
     */
    @FXML
    private Label descr;

    /**
     *
     */
    @FXML
    private ListView<DBObject> moves;

    /**
     * pokemon displayed in this window
     */
    private Pokemon pokemon;

    /**
     *
     * @param pokemon
     */
    public PokemonController(Pokemon pokemon){
        this.pokemon = pokemon;
    }

    /**
     *
     * @throws SQLException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    @Override
    public void initialize() throws SQLException, IOException, ClassNotFoundException {
        if( pokemon.getSprite() != null )
            pokeSprite.setImage(new Image(new FileInputStream(pokemon.getSprite())));

        Scaler s = new Scaler();

        pokeName.setText(pokemon.getName());
        //
        Image type1 = new Image(new FileInputStream(pokemon.getFirstType().getSprite()));
        Image type2 = null;

        // init of listviews
        evolutions.setCellFactory(PokemonSimpleCell.factory());
        moves.setCellFactory(MoveSimpleCell.factory());

        //

        s.add(pokeType1,type1,0.5);

        if( pokemon.getSecondType() != null ) {
            type2 = new Image(new FileInputStream(pokemon.getSecondType().getSprite()));

            s.add(pokeType2,type2,0.5);
        }

        this.fillAbilities(new ArrayList<>(pokemon.getAbilities().keySet()));
        //

        // pokemon egg groups
        fillEggGroup(pokemon.getFirstEggGroup(),eggGroup1);
        fillEggGroup(pokemon.getSecondEggGroup(),eggGroup2);
        //

        evolutions.getItems().addAll(pokemon.getEvolutions());

        // stats
        fillStat(pv,pv_label,pokemon.getHp());
        fillStat(atk,atk_label,pokemon.getAttack());
        fillStat(def,def_label,pokemon.getDefense());
        fillStat(spa,spa_label,pokemon.getSpeAtk());
        fillStat(spd,spd_label,pokemon.getSpeDef());
        fillStat(vit,vit_label,pokemon.getSpeed());
        //

        basestat.setText("Base statistique: "+Integer.toString(pokemon.getBaseStat()));

        descr.setText(pokemon.getDescription());

        moves.getItems().addAll(pokemon.getMoves());

        // windows double click opening listeners

        addTypeImageListener(pokeType1,pokemon.getFirstType(),pokemon.getSecondType());
        addTypeImageListener(pokeType2,pokemon.getFirstType(),pokemon.getSecondType());
        addPokemonListListener(evolutions);
        addMoveListListener(moves);
    }

    /**
     *
     * @param event
     */
    public void all(ActionEvent event) throws SQLException,IOException,ClassNotFoundException{
        moves.getItems().clear();
        moves.getItems().addAll(pokemon.getMoves());
    }

    /**
     *
     * @param event
     */
    public void level(ActionEvent event) throws SQLException,IOException,ClassNotFoundException{
        moves.getItems().clear();
        moves.getItems().addAll(pokemon.getLevelMoves().keySet());
    }

    /**
     *
     * @param event
     */
    public void egg(ActionEvent event) throws SQLException,IOException,ClassNotFoundException{
        moves.getItems().clear();
        moves.getItems().addAll(pokemon.getEggMoves());
    }

    /**
     *
     * @param event
     */
    public void tutor(ActionEvent event) throws SQLException,IOException,ClassNotFoundException{
        moves.getItems().clear();
        moves.getItems().addAll(pokemon.getTutorMoves());
    }

    /**
     *
     * @param event
     */
    public void ct(ActionEvent event) throws SQLException,IOException,ClassNotFoundException{
        moves.getItems().clear();
        moves.getItems().addAll(pokemon.getCtMoves());
    }

    /**
     *
     * @param event
     */
    public void dt(ActionEvent event) throws SQLException,IOException,ClassNotFoundException{
        moves.getItems().clear();
        moves.getItems().addAll(pokemon.getDtMoves());
    }

    /**
     *
     * @param event
     */
    public void event(ActionEvent event) throws SQLException,IOException,ClassNotFoundException{
        moves.getItems().clear();
        moves.getItems().addAll(pokemon.getEventMoves());
    }

    /**
     *
     * @param stat
     * @return
     */
    private static double bar(int stat){
        return ((double)stat) / 255.0;
    }

    /**
     *
     * @param stat
     * @return
     */
    private static String format(int stat){
        if(stat < 10 )
            return stat+"    ";

        if(stat < 100 )
            return stat+"  ";

        return Integer.toString(stat);
    }

    /**
     *
     * @param stat
     * @return
     */
    private static String cssColor(int stat){
        int red=511-4*stat;
        int green=4*stat;
        int blue=stat;

        return "rgb("+red+","+green+","+blue+")";
    }


    /**
     *
     * @param ablts
     */
    private void fillAbilities(List<Ability> ablts){
        if( ablts.size() > 0 ){
            ability1.setText(ablts.get(0).getName());
            addAbilityListener(ability1,ablts.get(0));

            if( ablts.size() > 1 ){
                ability2.setText(ablts.get(1).getName());
                addAbilityListener(ability2,ablts.get(1));

                if( ablts.size() > 2 ) {
                    abilityH.setText(ablts.get(2).getName());
                    addAbilityListener(abilityH,ablts.get(2));
                }else
                    abilityH.setText("");
            }else{
                ability2.setText("");
                abilityH.setText("");
            }
        }else{
            ability1.setText("");
            ability2.setText("");
            abilityH.setText("");
        }
    }

    /**
     *
     * @param eg
     * @param l
     */
    private static void fillEggGroup(EggGroup eg, Label l){
        if( eg != null ) {
            l.setText(eg.getName());
            addEggGroupListener(l,eg);
        }else
            l.setText("");
    }

    /**
     *
     * @param p
     * @param l
     * @param stat
     */
    private static void fillStat(ProgressBar p,Label l,int stat){
        p.setProgress(bar(stat));
        p.setStyle("-fx-accent: "+cssColor(stat)+";");
        l.setText(format(stat));
    }

}
