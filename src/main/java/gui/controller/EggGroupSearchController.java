package gui.controller;

import gui.StageManager;
import gui.view.EggGroupSimpleCell;
import javafx.event.EventHandler;
import javafx.scene.control.ListCell;
import javafx.scene.input.MouseEvent;
import model.EggGroup;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import static core.ListenerUtils.*;

/**
 *
 */
public class EggGroupSearchController extends SearchController {

    /**
     *
     * @throws SQLException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    @Override
    public void search() throws SQLException, IOException, ClassNotFoundException {
        List<EggGroup> le = EggGroup.findByNameLike(this.content.getText());

        this.results.getItems().clear();
        this.results.getItems().addAll(le);
    }

    /**
     *
     * @throws SQLException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    @Override
    public void initialize() throws SQLException, IOException, ClassNotFoundException {
        this.results.setCellFactory(EggGroupSimpleCell.factory());

        this.results.getItems().addAll(EggGroup.findAll());
        addEggGroupListListener(results);
    }
}
