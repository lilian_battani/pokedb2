package gui.view;

import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.util.Callback;
import model.DBObject;
import model.Pokemon;
import style.StyleManager;

import java.io.FileInputStream;
import java.io.IOException;

/**
 * pokemon search result
 */
public class PokemonSimpleCell extends ListCell<DBObject> {

    /**
     *
     * @param o
     * @param empty
     */
    @Override
    public void updateItem(DBObject o,boolean empty){
        super.updateItem(o,empty);

        Pokemon p = (Pokemon)o;

        if( p == null || empty ){
            this.setGraphic(null);
            return;
        }

        try {
            //
            HBox hb = new HBox();

            Label n = new Label(p.getName());
            ImageView iv = new ImageView(new Image(new FileInputStream(p.getSprite())));

            hb.getChildren().add(iv);
            hb.getChildren().add(n);

            StyleManager.hbox(hb);

            this.setGraphic(hb);
        }catch (IOException ioe){
            this.setGraphic(new Label(p.getName()));
        }catch (NullPointerException npe){
            this.setGraphic(new Label(p.getName()));
        }
    }

    /**
     *
     * @return
     */
    public static Callback<ListView<DBObject>,ListCell<DBObject>> factory(){
        return e -> new PokemonSimpleCell();
    }
}
