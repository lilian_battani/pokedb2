package gui.view;

import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;
import model.Ability;
import model.DBObject;
import model.EggGroup;

/**
 *
 */
public class EggGroupSimpleCell extends ListCell<DBObject> {

    /**
     *
     * @param o
     * @param empty
     */
    @Override
    public void updateItem(DBObject o, boolean empty){
        super.updateItem(o,empty);

        EggGroup eg = (EggGroup) o;

        if( eg == null || empty ){
            this.setGraphic(null);
            return;
        }

        Label n = new Label(eg.getName());

        this.setGraphic(n);
    }

    /**
     *
     * @return
     */
    public static Callback<ListView<DBObject>, ListCell<DBObject>> factory(){
        return e -> new EggGroupSimpleCell();
    }

}
