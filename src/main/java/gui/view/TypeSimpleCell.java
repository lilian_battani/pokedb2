package gui.view;

import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import model.Ability;
import model.DBObject;
import model.Type;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;

public class TypeSimpleCell extends ListCell<DBObject> {

    /**
     *
     * @param o
     * @param empty
     */
    @Override
    public void updateItem(DBObject o, boolean empty){
        super.updateItem(o,empty);

        Type t = (Type)o;
        double scale = 0.5;

        if( t == null || empty ){
            this.setGraphic(null);
            return;
        }

        HBox hb = new HBox();
        Label n = new Label(t.getName());

        try{
            Image im = new Image(new FileInputStream(t.getSprite()));
            ImageView iv = new ImageView(im);

            iv.setFitWidth(im.getWidth()*scale);
            iv.setFitHeight(im.getHeight()*scale);


            hb.getChildren().addAll(iv,n);

            this.setGraphic(hb);
        }catch (IOException ioe){
            this.setGraphic(new Label(t.getName()));
        }catch (NullPointerException npe){
            this.setGraphic(new Label(t.getName()));
        }

        hb.setSpacing(15);

        this.setGraphic(hb);
    }

    /**
     *
     * @return
     */
    public static Callback<ListView<DBObject>,ListCell<DBObject>> factory(){
        return e -> new TypeSimpleCell();
    }

}
