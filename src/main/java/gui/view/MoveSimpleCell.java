package gui.view;

import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import model.DBObject;
import model.Move;
import model.Pokemon;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;

public class MoveSimpleCell extends ListCell<DBObject> {

    /**
     *
     * @param o
     * @param empty
     */
    @Override
    public void updateItem(DBObject o,boolean empty){
        super.updateItem(o,empty);

        Move m = (Move)o;
        double scale = 0.5;

        if( m == null || empty ){
            this.setGraphic(null);
            return;
        }

        try {
            //
            HBox hb = new HBox();

            hb.setSpacing(15);

            Label n = new Label(m.getName());
            Label pow = new Label(Integer.toString(m.getPower()));
            Label prec = new Label(Integer.toString(m.getAccuracy())+" %");
            Label pp = new Label(Integer.toString(m.getPp())+" pp");
            Label status = new Label(Move.getStatusString(m.getStatus()));

            Image im = new Image(new FileInputStream(m.getType().getSprite()));
            ImageView iv = new ImageView(im);

            iv.setFitWidth(im.getWidth()*scale);
            iv.setFitHeight(im.getHeight()*scale);


            hb.getChildren().addAll(iv,n,pow,prec,pp,status);

            this.setGraphic(hb);
        }catch (IOException ioe){
            this.setGraphic(new Label(m.getName()));
        }catch (NullPointerException npe){
            this.setGraphic(new Label(m.getName()));
        }catch (SQLException sqle){
            this.setGraphic(new Label(m.getName()));
        }catch(ClassNotFoundException cnfe){
            this.setGraphic(new Label(m.getName()));
        }
    }

    /**
     *
     * @return
     */
    public static Callback<ListView<DBObject>,ListCell<DBObject>> factory(){
        return e -> new MoveSimpleCell();
    }
}
