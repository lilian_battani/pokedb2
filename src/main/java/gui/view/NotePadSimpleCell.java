package gui.view;

import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;
import model.Ability;
import model.DBObject;
import model.NotePad;

public class NotePadSimpleCell extends ListCell<DBObject> {
    /**
     *
     * @param o
     * @param empty
     */
    @Override
    public void updateItem(DBObject o, boolean empty){
        super.updateItem(o,empty);

        NotePad a = (NotePad) o;

        if( a == null || empty ){
            this.setGraphic(null);
            return;
        }

        Label n = new Label(a.getName());

        this.setGraphic(n);
    }

    /**
     *
     * @return
     */
    public static Callback<ListView<DBObject>,ListCell<DBObject>> factory(){
        return e -> new NotePadSimpleCell();
    }

}
