package gui.view;

import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import model.Ability;
import model.DBObject;
import model.Pokemon;

import java.io.FileInputStream;
import java.io.IOException;

/**
 *
 */
public class AbilitySimpleCell extends ListCell<DBObject> {

    /**
     *
     * @param o
     * @param empty
     */
    @Override
    public void updateItem(DBObject o, boolean empty){
        super.updateItem(o,empty);

        Ability a = (Ability)o;

        if( a == null || empty ){
            this.setGraphic(null);
            return;
        }

        Label n = new Label(a.getName());

        this.setGraphic(n);
    }

    /**
     *
     * @return
     */
    public static Callback<ListView<DBObject>,ListCell<DBObject>> factory(){
        return e -> new AbilitySimpleCell();
    }

}
