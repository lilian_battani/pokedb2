package gui;

import gui.controller.*;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.stage.Stage;
import javafx.util.Callback;
import model.*;

import java.io.IOException;

/**
 * class that manages page switches
 */
public class StageManager {

    /**
     * StageManager unique instance
     */
    private static StageManager INSTANCE;

    /**
     *
     */
    private Stage stage;

    /**
     *
     * @param stage
     */
    private StageManager(Stage stage){
        this.stage = stage;
    }

    /**
     *
     */
    public void home(){
        this.change("home.fxml",null,null);
        //

    }

    /**
     *
     */
    public void searchPokemon(){
        this.change("name_search.fxml",new PokemonSearchController(),null);
    }

    /**
     *
     */
    public void searchAbility(){
        this.change("name_search.fxml",new AbilitySearchController(),null);
    }

    /**
     *
     */
    public void searchEggGroup(){
        this.change("name_search.fxml",new EggGroupSearchController(),null);
    }

    /**
     *
     */
    public void searchMove(){
        this.change("name_search.fxml",new MoveSearchController(),null);
    }

    /**
     *
     */
    public void searchType(){
        this.change("name_search.fxml",new TypeSearchController(),null);
    }

    /**
     *
     */
    public void searchNotePad() {
       this.change("name_search.fxml",new NotePadSearchController(),null);
    }

    /**
     *
     */
    public void params(){
        this.change("db_window.fxml",new ImportController(),null);
    }

    /**
     *
     * @param a
     */
    public void ability(Ability a){
        this.change("ability.fxml",new AbilityController(a),null);
    }

    /**
     *
     * @param eg
     */
    public void eggGroup(EggGroup eg){
        this.change("egg_group.fxml",new EggGroupController(eg),null);
    }

    /**
     *
     * @param m
     */
    public void move(Move m){
        this.change("move.fxml",new MoveController(m),null);
    }

    /**
     *
     * @param p
     */
    public void pokemon(Pokemon p){
        this.change("pokemon.fxml",new PokemonController(p),null);
    }

    /**
     *
     * @param t
     */
    public void type(Type t){
        this.change("type.fxml",new TypeController(t,null),null);
    }

    /**
     *
     * @param t1
     * @param t2
     */
    public void type(Type t1,Type t2){
        this.change("type.fxml",new TypeController(t1,t2),null);
    }

    /**
     *
     * @param n
     */
    public void notepad(NotePad n){
        this.change("notepad.fxml",new NotePadController(n),null);
    }

    /**
     *
     */
    public void notepad(){
        this.change("notepad.fxml",new NotePadController(),null);
    }

    /**
     *
     * @param fxmlPage
     * @param controller
     * @param factory
     */
    private void change(String fxmlPage, Object controller, Callback<Class<?>,Object> factory){
       Parent root = null;
       Alert a = null;

       FXMLLoader loader = new FXMLLoader();

       loader.setLocation(this.getClass().getClassLoader().getResource(fxmlPage));

       if( factory != null )
           loader.setControllerFactory(factory);

       if( controller != null )
           loader.setController(controller);

       try{
           root = loader.load();
       }catch(IOException ioe){
           a = new Alert(Alert.AlertType.ERROR,"Erreur chargement "+fxmlPage+"\n"+ioe.getMessage());
           ioe.printStackTrace();
           a.showAndWait();
           return;
       }

       if( root == null )
           return;

       Scene scene = new Scene(root);

       scene.getStylesheets().addAll(this.getClass().getClassLoader().getResource("style.css").toExternalForm());

       this.stage.setScene(scene);
       this.stage.show();
    }

    /**
     *
     * @param s
     * @return
     */
    public static synchronized StageManager get(Stage s){
        if( INSTANCE == null )
            INSTANCE = new StageManager(s);

        return INSTANCE;
    }

    /**
     *
     * @return
     */
    public static synchronized StageManager get(){
        return StageManager.get(null);
    }
}
