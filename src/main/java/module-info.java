module pokedb2 {

    requires java.sql;

    requires javafx.fxml;
    requires javafx.base;
    requires javafx.controls;
    requires javafx.graphics;

    requires com.google.gson;
    requires java.desktop;
    requires sqlite.jdbc;

    exports pokedba.pokedb;

    opens gui.controller;
}