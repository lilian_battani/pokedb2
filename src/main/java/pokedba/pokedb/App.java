package pokedba.pokedb;

import core.ImageWriter;
import core.PokeDBConnection;
import gui.StageManager;
import gui.controller.MenuController;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Hello world!
 *
 */
public class App extends Application {

    /**
     *
     * @param args
     * @throws Exception
     */
    public static void main( String[] args ) throws Exception{
        launch(args);
    }

    /**
     *
     */
    @Override
    public void init() throws Exception{
        super.init();
    }

    /**
     *
     * @param stage
     * @throws Exception
     */
    @Override
    public void start(Stage stage) throws Exception {
        //
        FXMLLoader loader = new FXMLLoader();
        StageManager.get(stage);
        //
        StageManager.get().home();
    }

    /**
     *
     */
    @Override
    public void stop(){

    }
}
