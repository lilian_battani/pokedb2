-- pokemon abilities table
create table if not exists ability(
    id integer primary key autoincrement not null,
    name varchar(50) unique not null,
    description varchar(500) not null
);

-- pokemon egg groups
create table if not exists egg_group(
    id integer primary key autoincrement not null,
    name varchar(50) unique not null
);

-- pokemon types
create table if not exists type(
    id integer primary key autoincrement not null,
    name varchar(50) unique not null,
    sprite varchar(60)
);

-- type table (neutral, super efficient, resists, etc...)
create table if not exists type_table(
    attacker_id integer not null,
    defender_id integer not null,
    damage_multiplier double,

    primary key(attacker_id,defender_id),
    foreign key(attacker_id) references type(id),
    foreign key(defender_id) references type(id),
    check(damage_multiplier = 2 or damage_multiplier = 1 or damage_multiplier = 0.5 or damage_multiplier = 0)
);

-- pokemon moves
create table if not exists move(
    id integer primary key autoincrement not null,
    name varchar(50) unique not null,
    description varchar(500),
    power integer,
    accuracy integer,
    pp integer,
    status integer,
    type integer,
    foreign key(type) references type(id),
    check(power >= 0),
    check(accuracy between 0 and 100),
    check(pp > 0),
    check(status = 0 or status = 1 or status = 2)
);

-- pokemon table
create table if not exists pokemon(
    id integer primary key not null,
    name varchar(50) not null,
    base_pv integer not null,
    base_atk integer not null,
    base_def integer not null,
    base_spe_atk integer not null,
    base_spe_def integer not null,
    base_spd integer not null,
    first_type integer not null,
    second_type integer,
    first_egg_group integer,
    second_egg_group integer,
    sprite varchar(60),
    description varchar(1000),
    foreign key(first_type) references type(id),
    foreign key(second_type) references type(id),
    foreign key(first_egg_group) references egg_group(id),
    foreign key(second_egg_group) references egg_group(id),
    unique(name),
    check(base_pv > 0 and base_atk > 0 and base_def > 0 and base_spe_atk > 0 and base_spe_def > 0 and base_spd > 0),
    check(first_type != second_type),
    check(first_egg_group != second_egg_group or (first_egg_group is null and second_egg_group is null) )
);

-- pokemon evolutions
create table if not exists evolve(
    evolve_from integer not null,
    evolve_into integer not null,
    evolve_condition varchar(80) not null,
    primary key(evolve_from,evolve_into),
    foreign key(evolve_from) references pokemon(id),
    foreign key(evolve_into) references pokemon(id)
);

-- pokemon abilities
create table if not exists have_ability(
    pokemon integer not null,
    ability integer not null,
    hidden boolean not null,
    foreign key(pokemon) references pokemon(id),
    foreign key(ability) references ability(id)
);

-- pokemon moves
create table if not exists pokemon_move(
    pokemon integer not null,
    move integer not null,
    primary key(pokemon,move),
    foreign key(pokemon) references pokemon(id),
    foreign key(move) references move(id)
);

-- pokemon lvl moves
create table if not exists level_move(
    pokemon integer not null,
    move integer not null,
    lvl integer not null,
    primary key(pokemon,move),
    foreign key(pokemon) references pokemon_move(pokemon),
    foreign key(move) references pokemon_move(move),
    check(lvl between 0 and 100)
);

-- pokemon egg moves
create table if not exists egg_move(
    pokemon integer not null,
    move integer not null,
    primary key(pokemon,move),
    foreign key(pokemon) references pokemon_move(pokemon),
    foreign key(move) references pokemon_move(move)
);

-- pokemon tutor moves
create table if not exists tutor_move(
    pokemon integer not null,
    move integer not null,
    primary key(pokemon,move),
    foreign key(pokemon) references pokemon_move(pokemon),
    foreign key(move) references pokemon_move(move)
);

-- pokemon ct moves
create table if not exists ct_move(
    pokemon integer not null,
    move integer not null,
    primary key(pokemon,move),
    foreign key(pokemon) references pokemon_move(pokemon),
    foreign key(move) references pokemon_move(move)
);

-- pokemon dt moves
create table if not exists dt_move(
    pokemon integer not null,
    move integer not null,
    primary key(pokemon,move),
    foreign key(pokemon) references pokemon_move(pokemon),
    foreign key(move) references pokemon_move(move)
);

-- pokemon event moves
create table if not exists event_move(
    pokemon integer not null,
    move integer not null,
    primary key(pokemon,move),
    foreign key(pokemon) references pokemon_move(pokemon),
    foreign key(move) references pokemon_move(move)
);

-- pokedb notepads
create table if not exists notepad(
    id integer primary key autoincrement not null,
    name varchar(200),
    description varchar(10000)
);
